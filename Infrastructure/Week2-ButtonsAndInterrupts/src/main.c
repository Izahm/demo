
#include <util/delay.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <usart.h> 
#include <button.h>
#include <time.h>
#include <stdlib.h>
 /* This preprocessor directive makes sure that all the function declarations
                     * of the usart library are loaded.
                     * Check the tutorial of week 1: "1.8 Using your own library in VS Code" */

#define LED_PORT PORTB
#define BUTTON_PORT PORTC
#define BUTTON_PIN PINC
#define LED_DDR DDRB
#define BUTTON_DDR DDRC
#define BUTTON1 PC1
#define BUTTON2 PC2
#define BUTTON3 PC3
#define LED1 PB2
#define LED2 PB3
#define LED3 PB4
#define LED4 PB5


ISR( PCINT1_vect )
{

    
    if ( bit_is_clear( BUTTON_PIN, BUTTON1 )|| bit_is_clear( BUTTON_PIN, BUTTON2 ))
    {
        //debounce
        _delay_us( 1000 );

            if ( bit_is_clear( BUTTON_PIN, BUTTON1 ))
            {
                LED_PORT &= ~_BV( LED3 )& ~_BV( LED4 ); 
                 _delay_ms( 2000 );
                  LED_PORT |= _BV( LED3 ) | _BV( LED4); 
                 _delay_ms( 2000  );
            }
            else if(bit_is_clear( BUTTON_PIN, BUTTON2 )){
           for (int i = 0; i< 7; i++){  LED_PORT &= ~_BV( LED2 ); 
                 _delay_ms( 500 );
                  LED_PORT |= _BV( LED2 ); 
                 _delay_ms( 500  );}
            
            }
             
   }
   
}



int main(int argc, char const *argv[])
{
    initUSART();
    LED_DDR |= _BV( LED1 )|_BV( LED2 )|_BV( LED3 )|_BV( LED4 );   
    LED_PORT |= _BV( LED1 ) | _BV( LED2 ) | _BV( LED3 ) | _BV( LED4 );
    BUTTON_PORT|= _BV(BUTTON1)|_BV(BUTTON2)|_BV(BUTTON3); //Enable pullup
    PCICR |= _BV( PCIE1 ); // interrupts from PORT C
    PCMSK1 |= _BV( BUTTON1 )|_BV( BUTTON2 )|_BV( BUTTON3); //interested in all buttons

    sei(); //activate the interrupt sys
while(1){

                 LED_PORT &= ~_BV( LED1 )& ~_BV( LED2 ); 
                 _delay_ms( 2000 );
                  LED_PORT |= _BV( LED1 ) | _BV( LED2 ); 
                 _delay_ms( 2000  );
            
}
        
             
    return 0;
}









/* #define LED_PORT PORTB
#define BUTTON_PORT PORTC
#define BUTTON_PIN PINC
#define LED_DDR DDRB
#define BUTTON_DDR DDRC
#define BUTTON1 PC1
#define BUTTON2 PC2
#define BUTTON3 PC3
#define LED1 PB2
#define LED2 PB3
#define LED3 PB4
#define LED4 PB5

int button_pushed=1;

void printRandoms(int lower, int upper, 
                             int count)
{
    int i;
    for (i = 0; i < count; i++) {
        int num = (rand() %
           (upper - lower + 1)) + lower;
        printf("%d ", num);
    }
}
 

ISR( PCINT1_vect )
{

    button_pushed=1;
    initUSART();
while(1){
    if ( bit_is_clear( BUTTON_PIN, BUTTON1 )||bit_is_clear( BUTTON_PIN, BUTTON2 )||bit_is_clear( BUTTON_PIN, BUTTON3 ))
    {
        //debounce
        _delay_us( 1000 );

            if ( bit_is_clear( BUTTON_PIN, BUTTON1 )||bit_is_clear( BUTTON_PIN, BUTTON2 )||bit_is_clear( BUTTON_PIN, BUTTON3 ))
            {
                button_pushed=0; 
            }
            else{
                button_pushed=1;               
              }
             
   }
   }
}



int main(int argc, char const *argv[])
{
    initUSART();
    LED_DDR |= _BV( LED4 );   // we'll use led1 and led2
    LED_PORT |= _BV( LED4 );  //turn one led off
    BUTTON_PORT|= _BV(BUTTON1)|_BV(BUTTON2)|_BV(BUTTON3); //Enable pullup
    PCICR |= _BV( PCIE1 ); // interrupts from PORT C
    PCMSK1 |= _BV( BUTTON1 )|_BV( BUTTON2 )|_BV( BUTTON3); //interested in all buttons

    sei(); //activate the interrupt sys
while(1){   
    while(button_pushed){   
                  LED_PORT &= ~_BV( LED4 );   // turn led1 on
                 _delay_ms( 100 );
                  LED_PORT |= _BV( LED4 );    // turn led1 off
                 _delay_ms( 100 );
                 printf("Value %d", button_pushed);
                }
       
        }
        
             
    return 0;
}


 */