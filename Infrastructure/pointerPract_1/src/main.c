#include <util/delay.h>
#include <avr/io.h>
#include <usart.h>
#include <string.h>

#define MAX 5
#define SEVENFOLD(num) num*7

void increment_byref( int* a)
{
    printf( "Incrementing a (%d)\n", *a);
    (*a)++;
}

void increment_byval( int a )
{
    printf( "Incrementing a (%d)\n", a );
    a++;
}

void printArray(int arr[]){
 for(int i  = 0; i< MAX; i++)
      printf("Address: %p has value: %d\n", &arr[i], arr[i]);
}

void makeArray(int arr[])
{
for(int i  = 0; i< MAX; i++){
      arr[i]= SEVENFOLD(i);
}


}

int main()
{
    initUSART();
     int numbers[MAX] = {0};
     makeArray(numbers);
     printArray(numbers);
    return 0;
}