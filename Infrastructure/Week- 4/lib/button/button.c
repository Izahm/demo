
#include <util/delay.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <usart.h>
#include <stdlib.h>

#define BUTTON_PORT PORTC
#define BUTTON_PIN PINC
#define BUTTON_DDR DDRC
#define BUTTON1 PC1
#define BUTTON2 PC2
#define BUTTON3 PC3


void enableButton( int button ){
    if (button <1 | button >3) return;
     DDRC &= ~( 1 << (PC1+ (button-1))); 
 }

int buttonPushed( int button ){// can button be PC1 for example? 
if (button <1 | button >3) return;
return bit_is_clear(PINC, (PC1+ (button-1)));
}

int buttonReleased( int button ){
    if (button <1 | button >3) return;
    return !bit_is_clear(PINC, (PC1+ (button-1)));
}

int isAnyClicked(){
    return bit_is_clear( BUTTON_PIN, BUTTON1 )||bit_is_clear( BUTTON_PIN, BUTTON2 )||bit_is_clear( BUTTON_PIN, BUTTON3 );
}

void initBTN(){
    BUTTON_DDR &= ~_BV( BUTTON1 ) &~_BV( BUTTON2 ) &~_BV( BUTTON3 );                   
    PORTC |= ( 1 << PC1 )|( 1 << PC2 )|( 1 << PC3 );  
}

//enable interrupts on buttons
void enableBTNinterrupt(){
    PCICR |= _BV( PCIE1 );                         
    PCMSK1 |=  _BV( BUTTON1 ) | _BV(BUTTON2)| _BV(BUTTON3); 
}