#include <util/delay.h> 
#include <avr/io.h> 

void enableLed  (uint8_t leds);

void lightUpLed ( uint8_t led );

void lightDownLed ( uint8_t led );

//enable the leds
void enableLeds();

//disable the leds
void disableLeds();

//long duration light up
void extendedLightup();

//short duration light up
void shortenedLightup();
void lightUpAllLeds();
void lightOffAllLeds();

//countdown before morse code starts
void countdown();
