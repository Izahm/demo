

void morseCode(char word[]);  

//letters
void aCode();
void bCode();
void cCode();
void dCode();
void eCode();
void fCode();
void gCode();
void hCode();
void iCode();
void jCode();
void kCode();
void lCode();
void mCode();
void nCode();
void oCode();
void pCode();
void qCode();
void rCode();
void sCode();
void tCode();
void uCode();
void vCode();
void wCode();
void xCode();
void yCode();
void zCode();

//numbers
void zeroCode();
void oneCode();
void twoCode();
void threeCode();
void fourCode();
void fiveCode();
void sixCode();
void sevenCode();
void eightCode();
void nineCode();
void morseDance();

