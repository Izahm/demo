#include <util/delay.h> 
#include <avr/io.h> 
#include <led.h> 

void enableLed  (uint8_t leds)
{
    DDRB = leds;  
}

void lightUpLed ( uint8_t led )    //Note: enabled LEDs light up immediately ( 0 = on )
{
    //DDRB= 0B00110000
    if(led == 0B00100000){
       PORTB = ( 0 << ( PB2 + 3 ));
    }
    if(led == 0B00010000){
       PORTB = ( 0 << ( PB2 + 2 ));
    }
    if(led == 0B00001000){
        PORTB = ( 0 << ( PB2 + 1 ));
    }
    if(led == 0B00000100){
       PORTB = ( 0 << ( PB2 ));
    }
   
}

void lightDownLed ( uint8_t led )
{
if(led == 0B00100000){
       PORTB = ( 1 << ( PB2 + 3 ));
    }
    if(led == 0B00010000){
       PORTB = ( 1 << ( PB2 + 2 ));
    }
    if(led == 0B00001000){
       PORTB = ( 1 << ( PB2 + 1 ));
    }
    if(led == 0B00000100){
      PORTB = ( 1 << ( PB2 ));
    }
}



//  a function dimLed() to the led library that will dim the led during a certain period (in milliseconds) to a certain percentage.
void dimLeds (int percentage, int duration){
//enable the leds
    DDRB=0b00111100;
    while (1){
         PORTB |= ( 0 << ( PB2  )); 
         PORTB |= ( 0 << ( PB2 + 1 )); 
         PORTB |= ( 0 << ( PB2 + 2 )); 
         PORTB |= ( 0 << ( PB2 + 3 )); 
        _delay_ms(percentage*duration/100); 
         PORTB |= ( 1 << ( PB2 + 3 )); 
         PORTB |= ( 1 << ( PB2 + 2 )); 
         PORTB |= ( 1 << ( PB2 + 1 )); 
         PORTB |= ( 1 << ( PB2 )); 
        _delay_ms(((100-percentage)*duration)/100);
    }
}
//enable the leds
void enableLeds(){
DDRB= 0B00111100;
}
//disable the leds
void disableLeds(){
 DDRB= 0B00000000;   
}
//long duration light up
void extendedLightup(){
        lightUpAllLeds();
        _delay_ms(2000);
}
//short duration light up
void shortenedLightup(){
        lightUpAllLeds();
        _delay_ms(200);
}

void lightUpAllLeds(){
        PORTB = (0 << (PB2));
        PORTB |= (0 << (PB2 + 1));
        PORTB |= (0 << (PB2 + 2));
        PORTB |= (0 << (PB2 + 3));
}
void lightOffAllLeds(){
        PORTB = (1 << (PB2));
        PORTB |= (1 << (PB2 + 1));
        PORTB |= (1 << (PB2 + 2));
        PORTB |= (1 << (PB2 + 3));
        _delay_ms(200);
}

void countdown(){

    enableLeds();
        //light up all 4 leds
        PORTB = (0 << (PB2));
        PORTB |= (0 << (PB2 + 1));
        PORTB |= (0 << (PB2 + 2));
        PORTB |= (0 << (PB2 + 3));
        //after every second we turn off one led (counting down)
        _delay_ms(1000); 
        PORTB = (1 << (PB2+3));
        _delay_ms(1000);
        PORTB |= (1 << (PB2+2));
       _delay_ms(1000);
        PORTB |= (1 << (PB2+1));
        _delay_ms(1000);
        PORTB |= (1 << (PB2));
}
