#include<stdio.h>
#include <util/delay.h> 
#include <avr/io.h> 
#include <string.h> 

void morseCode(char word[]){   
countdown();
int i = 0;
while(i<strlen(word)){
 switch (toupper(word[i]))
{
    case 'A':
    aCode();
    _delay_ms(500);
    break;
     case 'B':
    bCode();
    _delay_ms(500);
    break;
     case 'C':
    cCode();
    _delay_ms(500);
    break;
     case 'D':
    dCode();
    _delay_ms(500);
    break;
     case 'E':
    eCode();
    _delay_ms(500);
    break;
     case 'F':
    fCode();
    _delay_ms(500);
    break;
     case 'G':
    gCode();
    _delay_ms(500);
    break;
     case 'H':
    hCode();
    _delay_ms(500);
    break;
     case 'I':
    iCode();
    _delay_ms(500);
    break;
     case 'J':
    jCode();
    _delay_ms(500);
    break;
     case 'K':
    kCode();
    _delay_ms(500);
    break;
     case 'L':
    lCode();
    _delay_ms(500);
    break;
     case 'M':
    mCode();
    _delay_ms(500);
    break;
     case 'N':
    nCode();
    _delay_ms(500);
    break;
     case 'O':
    oCode();
    _delay_ms(500);
    break;
     case 'P':
    pCode();
    _delay_ms(500);
    break;
     case 'Q':
    qCode();
    _delay_ms(500);
    break;
     case 'R':
    rCode();
    _delay_ms(500);
    break;
     case 'S':
    sCode();
    _delay_ms(500);
    break;
     case 'T':
    tCode();
    _delay_ms(500);
    break;
     case 'U':
    uCode();
    _delay_ms(500);
    break;
     case 'V':
    vCode();
    _delay_ms(500);
    break;
    case 'W':
    wCode();
    _delay_ms(500);
    break;
    case 'X':
    xCode();
    _delay_ms(500);
    break;
    case 'Y':
    yCode();
    _delay_ms(500);
    break;
    case 'Z':
    zCode();
    _delay_ms(500);
    break;
    case '0':
    zeroCode();
    _delay_ms(500);
    break;
    case '1':
    oneCode();
    _delay_ms(500);
    break;
    case '2':
    twoCode();
    _delay_ms(500);
    break;
    case '3':
    threeCode();
    _delay_ms(500);
    break;
    case '4':
    fourCode();
    _delay_ms(500);
    break;
    case '5':
    fiveCode();
    _delay_ms(500);
    break;
    case '6':
    sixCode();
    _delay_ms(500);
    break;
    case '7':
    sevenCode();
    _delay_ms(500);
    break;
    case '8':
    eightCode();
    _delay_ms(500);
    break;
    case '9':
    nineCode();
    _delay_ms(500);
    break;
default:
    break;
}
    i++;
    if(i==10 || i==strlen(word)){
        morseDance();
    }
}
}

void aCode(){
//light up all 4 leds
    shortenedLightup();
    lightOffAllLeds();
    extendedLightup();
    lightOffAllLeds();
}

void bCode(){
    extendedLightup();
    lightOffAllLeds();
    shortenedLightup();
    lightOffAllLeds();
    shortenedLightup();
    lightOffAllLeds();
    shortenedLightup();
    lightOffAllLeds();
}

void cCode(){
    extendedLightup();
    lightOffAllLeds();
    shortenedLightup();
    lightOffAllLeds();
    extendedLightup();
    lightOffAllLeds();
    shortenedLightup();
    lightOffAllLeds();
}

void dCode(){
    extendedLightup();
    lightOffAllLeds();  
    shortenedLightup();
    lightOffAllLeds(); 
    shortenedLightup();
    lightOffAllLeds();
}

void eCode(){
    shortenedLightup();
    lightOffAllLeds();
}

void fCode(){
    shortenedLightup();
    lightOffAllLeds();
    shortenedLightup();
    lightOffAllLeds();
    extendedLightup();
    lightOffAllLeds();
    shortenedLightup();
    lightOffAllLeds();
}

void gCode(){
    extendedLightup();
    lightOffAllLeds();
    extendedLightup();
    lightOffAllLeds();
    shortenedLightup();
    lightOffAllLeds();
}

void hCode(){
    shortenedLightup();
    lightOffAllLeds();
    shortenedLightup();
    lightOffAllLeds();
    shortenedLightup();
    lightOffAllLeds();
    shortenedLightup();
    lightOffAllLeds();
}
void iCode(){
    shortenedLightup();
    lightOffAllLeds();
    shortenedLightup();
    lightOffAllLeds();
}
void jCode(){
    shortenedLightup();
    lightOffAllLeds();
    extendedLightup();
    lightOffAllLeds();
    extendedLightup();
    lightOffAllLeds();
    extendedLightup();
    lightOffAllLeds();
}
void kCode(){
    extendedLightup();
    lightOffAllLeds();
    shortenedLightup();
    lightOffAllLeds();
    extendedLightup();
    lightOffAllLeds();
}
void lCode(){
    shortenedLightup();
    lightOffAllLeds();
    extendedLightup();
    lightOffAllLeds();
    shortenedLightup();
    lightOffAllLeds();
    shortenedLightup();
    lightOffAllLeds();
}

void mCode(){
    extendedLightup();
    lightOffAllLeds();
    extendedLightup();
    lightOffAllLeds();
}
void nCode(){
    extendedLightup();
    lightOffAllLeds();
    shortenedLightup();
    lightOffAllLeds();
}

void oCode(){
    extendedLightup();
    lightOffAllLeds();
    extendedLightup();
    lightOffAllLeds();
    extendedLightup();
    lightOffAllLeds();
}

void pCode(){
    shortenedLightup();
    lightOffAllLeds();
    extendedLightup();
    lightOffAllLeds();
    extendedLightup();
    lightOffAllLeds();
    shortenedLightup();
    lightOffAllLeds();
}

void qCode(){
    extendedLightup();
    lightOffAllLeds();
    extendedLightup();
    lightOffAllLeds();
    shortenedLightup();
    lightOffAllLeds();
    extendedLightup();
    lightOffAllLeds();
}

void rCode(){
    shortenedLightup();
    lightOffAllLeds();
    extendedLightup();
    lightOffAllLeds();
    shortenedLightup();
    lightOffAllLeds();
}

void sCode(){
    shortenedLightup();
    lightOffAllLeds();
    shortenedLightup();
    lightOffAllLeds();
    shortenedLightup();
    lightOffAllLeds();
}

void tCode(){
    extendedLightup();
    lightOffAllLeds();
}

void uCode(){
    shortenedLightup();
    lightOffAllLeds();
    shortenedLightup();
    lightOffAllLeds();
    extendedLightup();
    lightOffAllLeds();
}

void vCode(){
    shortenedLightup();
    lightOffAllLeds();
    shortenedLightup();
    lightOffAllLeds();
    shortenedLightup();
    lightOffAllLeds();
    extendedLightup();
    lightOffAllLeds();
}

void wCode(){
    shortenedLightup();
    lightOffAllLeds();
    extendedLightup();
    lightOffAllLeds();
    extendedLightup();
    lightOffAllLeds();
}

void xCode(){
   extendedLightup();
   lightOffAllLeds();
   shortenedLightup();
   lightOffAllLeds();
   shortenedLightup();
   lightOffAllLeds();
   extendedLightup();
   lightOffAllLeds();
}

void yCode(){
   extendedLightup();
   lightOffAllLeds();
   shortenedLightup();
   lightOffAllLeds(); 
   extendedLightup();
   lightOffAllLeds();
   extendedLightup();
   lightOffAllLeds();
}

void zCode(){
   extendedLightup();
   lightOffAllLeds(); 
   extendedLightup();
   lightOffAllLeds(); 
   shortenedLightup();
   lightOffAllLeds(); 
   shortenedLightup();
   lightOffAllLeds(); 
}

void zeroCode(){
   extendedLightup();
   lightOffAllLeds(); 
   extendedLightup();
   lightOffAllLeds(); 
   extendedLightup();
   lightOffAllLeds(); 
   extendedLightup();
   lightOffAllLeds(); 
   extendedLightup();
   lightOffAllLeds(); 
}


void oneCode(){
   shortenedLightup();
   lightOffAllLeds(); 
   extendedLightup();
   lightOffAllLeds(); 
   extendedLightup();
   lightOffAllLeds(); 
   extendedLightup();
   lightOffAllLeds(); 
   extendedLightup();
   lightOffAllLeds(); 
}

void twoCode(){
   shortenedLightup();
   lightOffAllLeds(); 
   shortenedLightup();
   lightOffAllLeds(); 
   extendedLightup();
   lightOffAllLeds();
   extendedLightup();
   lightOffAllLeds();
   extendedLightup();
   lightOffAllLeds();
}

void threeCode(){
   shortenedLightup();
   lightOffAllLeds(); 
   shortenedLightup();
   lightOffAllLeds(); 
   shortenedLightup();
   lightOffAllLeds(); 
   extendedLightup();
   lightOffAllLeds();
   extendedLightup();
   lightOffAllLeds();
}

void fourCode(){
   shortenedLightup();
   lightOffAllLeds();
   shortenedLightup();
   lightOffAllLeds();
   shortenedLightup();
   lightOffAllLeds();
   shortenedLightup();
   lightOffAllLeds();
   extendedLightup();
   lightOffAllLeds();
}

void fiveCode(){
   shortenedLightup();
   lightOffAllLeds();
   shortenedLightup();
   lightOffAllLeds();
   shortenedLightup();
   lightOffAllLeds();
   shortenedLightup();
   lightOffAllLeds();
   shortenedLightup();
   lightOffAllLeds();
}

void sixCode(){
   extendedLightup();
   lightOffAllLeds();
   shortenedLightup();
   lightOffAllLeds();
   shortenedLightup();
   lightOffAllLeds();
   shortenedLightup();
   lightOffAllLeds();
   shortenedLightup();
   lightOffAllLeds();
}

void sevenCode(){
   extendedLightup();
   lightOffAllLeds();
   extendedLightup();
   lightOffAllLeds();
   shortenedLightup();
   lightOffAllLeds();
   shortenedLightup();
   lightOffAllLeds();
   shortenedLightup();
   lightOffAllLeds();
}

void eightCode(){
   extendedLightup();
   lightOffAllLeds();
   extendedLightup();
   lightOffAllLeds();
   extendedLightup();
   lightOffAllLeds();
   shortenedLightup();
   lightOffAllLeds();
   shortenedLightup();
   lightOffAllLeds();
}

void nineCode(){
   extendedLightup();
   lightOffAllLeds();
   extendedLightup();
   lightOffAllLeds();
   extendedLightup();
   lightOffAllLeds();
   extendedLightup();
   lightOffAllLeds();
   shortenedLightup();
   lightOffAllLeds();
}

void danceQuore(){
     PORTB= 0b00001000;
    PORTB|= 0b00000100;
    _delay_ms(200);
    PORTB= 0b00000000;
    PORTB|= 0b00000000;
    _delay_ms(200);
    PORTB= 0b00001000;
    PORTB|= 0b00000100;
     _delay_ms(200);
    DDRB=0b00000000;
    _delay_ms(200);
    DDRB=0b00001100;
    enableLeds();
    _delay_ms(200);  
    PORTB= 0b00001000;
    PORTB|= 0b00000100;
    _delay_ms(200);
    PORTB= 0b00000000;
    PORTB|= 0b00000000;
    _delay_ms(200);
    PORTB= 0b00001000;
    PORTB|= 0b00000100;
     _delay_ms(200);
     DDRB=0b00000000;
}

void morseDance(){
     danceQuore();
     enableLeds();
     shortenedLightup();
     lightOffAllLeds();
     shortenedLightup();
     lightOffAllLeds();
     shortenedLightup();
     lightOffAllLeds();
     shortenedLightup();
     lightOffAllLeds();
}
