#include <util/delay.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <usart.h>
#include <led.h>

#define LED_PORT PORTB
#define BUTTON_PORT PORTC
#define BUTTON_PIN PINC
#define LED_DDR DDRB
#define BUTTON_DDR DDRC
#define BUTTON1 PC1
#define BUTTON2 PC2
#define BUTTON3 PC3
#define LED1 PB2
#define LED2 PB3
#define LED3 PB4
#define LED4 PB5

#define DELAY(p) _delay_ms(p)


uint8_t puzzleMemory[]={0};
uint8_t randomPattern[10]={0};




void byRef( int* button, int value)
{
    *button += value;
}

void incByRef(int* num){
(*num)++;

}

int isAnyClicked(){
    return bit_is_clear( BUTTON_PIN, BUTTON1 )||bit_is_clear( BUTTON_PIN, BUTTON2 )||bit_is_clear( BUTTON_PIN, BUTTON3 );
}



ISR( PCINT1_vect )
{
    // is any button clicked?
    if ( isAnyClicked())
    {
      //debounce
        _delay_us( 1000 );
        
        if ( isAnyClicked())
        {          
                 generatePuzzle(randomPattern, 10);
                 readInput(randomPattern,sizeof(randomPattern)/sizeof(int));
        }
    }
}


int main()
{
    LED_DDR |= _BV( LED4 )|_BV( LED3 )|_BV( LED2 )|_BV( LED1 );   
    LED_PORT |= _BV( LED4 )|_BV( LED3 )|_BV( LED2 )|_BV( LED1 );  
    BUTTON_DDR &= ~_BV( BUTTON1 ) &~_BV( BUTTON2 ) &~_BV( BUTTON3 );  //Connect buttons          
    BUTTON_PORT |= _BV( BUTTON1 ) | _BV(BUTTON2)| _BV(BUTTON3);  //Pull up        
    PCICR |= _BV( PCIE1 );  
      //00000000 &                       
                            
    PCMSK1 |=  _BV( BUTTON1 ) | _BV(BUTTON2)| _BV(BUTTON3); 
    sei();  

initUSART();

int seed =0;
while(1){   
                  //printf("COUNTER: %d \n", seed);
                  LED_PORT &= ~_BV( LED4 );   // turn led1 on
                 _delay_ms( 100 );
                  LED_PORT |= _BV( LED4 );    // turn led1 off
                 _delay_ms( 100 );
                 seed++;
                 if (seed == 150){
                     seed =0;
                 }
                 srand(seed);

        }     
return 0;
}


void printPuzzleMemory(){
  printf("[");
          for (int i = 0; i < 5; i++) {
                    printf("%d ", puzzleMemory[i]);
                }
    printf("]");
    printf("\n");
}

void playPuzzle(uint8_t arr[], int num){
for(int i =0; i<num; i++){ // [0 2 0 1 2 0 2 0 0 1]
        puzzleMemory[i]=arr[i];
        PORTB = ~_BV((PB2+arr[i])); //PB2+1 = PB3 // PB2+0 = PB2  LED 1
        DELAY(500);
        PORTB |= _BV((PB2+arr[i]));
        DELAY(700);
     }            
}

void printPuzzle(uint8_t arr[],int arrSize){
    printf("[");
          for (int i = 0; i < arrSize; i++) {
                    printf("%d ", arr[i]);
                }
    printf("]");
    printf("\n");
}


void generatePuzzle(uint8_t arr[], int arrSize)
{
    int i;
    for (i = 0; i < arrSize; i++) {
        int num = (rand() % 3);
           arr[i]=num;
    }
}


void readInput(uint8_t arr[], int arrSize){
  
  int playerTurn;
  int button_pushed; 
  int currentPlay;
  int userInput;
  DELAY(1000);

  for (int level=1; level <=arrSize; level++) // Number of levels to play
    {
      playPuzzle(randomPattern, level);  
      playerTurn = 1;
      button_pushed = 0;
      userInput = 0;
      
      while (playerTurn < level) {

        currentPlay = puzzleMemory[playerTurn - 1];

        while (button_pushed == 0) {
           if (bit_is_clear( BUTTON_PIN, BUTTON1 )) // Button 1 Pressed
           {
             button_pushed = 1;
             userInput = 0;
           }
           if (bit_is_clear( BUTTON_PIN, BUTTON2 )) // Button 2 Pressed
           {
             button_pushed = 1;
             userInput = 1;
           }
           if (bit_is_clear( BUTTON_PIN, BUTTON3 )) // Button 3 Pressed
           {
             button_pushed = 1;
             userInput = 2;
           }
           if (button_pushed == 1) // A button was Pressed
           {
             if (currentPlay == userInput) // right button pushed (move to next play)
             {
                 printf("You pressed the right button!");
               playerTurn++;
             }
             else // wrong button pushed
             {
               game_over(0);
             }
          }  
        }

    button_pushed = 0;
   }

  } 
    game_over(1);
  
 }

void game_over(int win) {
  if (win==1) {
    printf("\nYou Win!");
  }
  
  else {
      printf("\nYou Lose!");
  }
   printf("\nGame over");
}




