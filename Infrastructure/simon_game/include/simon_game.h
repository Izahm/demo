
#include <util/delay.h> 
#include <avr/io.h> 


void byRef( int* button, int value);
void incrbyRef( int* num, int value);
void printPuzzleMemory();
void playPuzzle(uint8_t arr[], int num);
void printPuzzle(uint8_t arr[],int arrSize);
void generatePuzzle(uint8_t arr[], int arrSize);
int isAnyClicked();
int readInput(uint8_t arr[], int length);

