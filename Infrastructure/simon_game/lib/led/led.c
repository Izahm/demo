#include <util/delay.h> 
#include <avr/io.h> 
#include <led.h> 

void enableLed  (uint8_t leds)
{
    DDRB = leds;  
}

void lightUpLed ( uint8_t led )    
{
       PORTB |= (0 << (PB2+ led));
}

void lightDownLed ( uint8_t led )
{       
       PORTB ^= led;
}


//  a function dimLed() to the led library that will dim the led during a certain period (in milliseconds) to a certain percentage.
void dimLeds (int percentage, int duration){
//enable the leds
    DDRB=0b00111100;
    while (1){
         lightsUp();
        _delay_ms(percentage*duration/100); 
         lightsOff();
        _delay_ms(((100-percentage)*duration)/100);
    }
}
//enable all the leds
void enableLeds(){
DDRB= 0b00111100;
}
//disable the leds
void disableLeds(){
 DDRB= 0b00000000;   
}
//long duration light up
void longLightUp(){
        lightsUp();
        _delay_ms(2000);
}
//short duration light up
void shortLightUp(){
        lightsUp();
        _delay_ms(200);
}

void lightsUp(){
        PORTB = (0 << (PB2));  //~_BV(PB2);
        PORTB |= (0 << (PB2 + 1));
        PORTB |= (0 << (PB2 + 2));
        PORTB |= (0 << (PB2 + 3));
}
void lightsOff(){
        PORTB = _BV(PB2); 
        PORTB |= _BV (PB2 + 1);
        PORTB |= _BV(PB2 + 2);
        PORTB |= _BV(PB2 + 3);
        _delay_ms(200);
}

void countdown(){

    enableLeds();
        //light up all 4 leds
        lightsUp();
        //after every second we turn off one led (counting down)
        _delay_ms(1000); 
        PORTB = (1 << (PB2+3));
        _delay_ms(1000);
        PORTB |= (1 << (PB2+2));
       _delay_ms(1000);
        PORTB |= (1 << (PB2+1));
        _delay_ms(1000);
        PORTB |= (1 << (PB2));
}
