#include <avr/io.h>

void enableBuzzer();
void disableBuzzer();
void playTone( float frequency, uint32_t duration );
