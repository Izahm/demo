#include <util/delay.h>
#include <string.h>
#include <usart.h>
#include <display.h>
#include <stdlib.h>
#include <avr/io.h>
#include <avr/interrupt.h>



#define LED_PORT PORTB
#define BUTTON_PORT PORTC
#define BUTTON_PIN PINC
#define LED_DDR DDRB
#define BUTTON_DDR DDRC
#define BUTTON1 PC1
#define BUTTON2 PC2
#define BUTTON3 PC3
#define LED1 PB2
#define LED2 PB3
#define LED3 PB4
#define LED4 PB5

/* Write a function void writeLinesOnHeap(char sentence []) 
that will now put the sentence on the heap:

Allocate just enough space on the heap to hold the sentence. 
You need to find the length of the string for this; 
you may use string functions from string.h. Don't forget the closing zero!
Use a string function from string.h to copy the string to the allocated place on the heap.
Now adjust your main loop (in the main) 
so that the above function is used to copy the string to the heap each time.
What do you notice?
 */

int memoryValue;
void writeLinesOnHeap(char sentence[]){
char* sentenceLoc;
sentenceLoc = (char*)malloc(strlen(sentence)*sizeof(char));
memoryValue += sizeof(sentenceLoc);
strcpy(sentenceLoc, sentence);
free(sentenceLoc);
}

void initADC()
{
    ADMUX |= ( 0 << ADPS2 ) | ( 0 << ADPS1 ) | ( 1 << ADPS0 );//dividing the voltage by 2
    ADMUX |= ( 1 << REFS0 );    //Set up of reference voltage. We choose 5V as reference.
    ADCSRA |= ( 1 << ADPS2 ) | ( 1 << ADPS1 ) | ( 1 << ADPS0 );  //Determine a sample rate by setting a division factor. Used division factor: 128
    ADCSRA |= ( 1 << ADEN ); //Enable the ADC
}

int main()
{
  initDisplay();
  initADC();
  initUSART();
  
    DDRC &= ~( 1 << PC1 );                   
    PORTC |= ( 1 << PC1 );  
 
 while (1)
 {
    if (( PINC & ( 1 << PC1 )) == 0 )   
        {
                  for(int i =0;i<100;i++){           
                   printf("%d: I am not allowed to speak in class. \n",i+1);
                    writeLinesOnHeap("I am not allowed to speak in class.\" is on heap\n");
                    printf("%d bytes are now occupied on heap\n",memoryValue);
                   }      
        }
 }
 
   
    return 0;
}





/*  while ( 1 )
    {
        printf( "\nPINC: " );
        if (( PINC & ( 1 << PC1 )) == 0 )   
                                            
        {
            writeNumber(PINC & ( 1 << PC1 ));
        }
        else
        {
            writeNumber(PINC & ( 1 << PC1 ));
        }
        _delay_ms( 1000 );
    } */



/* 
while (1)
 {
    if (( PINC & ( 1 << PC1 )) == 0 )   
        {
                  for(int i =0;i<100;i++){           
                   printf("%d: I am not allowed to speak in class. \n",i+1);
                    writeLinesOnHeap("I am not allowed to speak in class.\" is on heap\n");
                    printf("%d bytes are now occupied on heap\n",memoryValue);
                   }      
        }
 } */




/* int starter = 1234;
for(int i = 0; i<10; i++){
writeNumberAndWait(starter,3000);
starter+=1111;
}
 */
