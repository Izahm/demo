
#include <util/delay.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <usart.h>

void enableButton( int button ){
    if (button <1 | button >3) return;
     DDRC &= ~( 1 << (PC1+ (button-1))); 
 }

int buttonPushed( int button ){// can button be PC1 for example? 
return bit_is_clear(PINC, (PC1+ (button-1)));
}

int buttonReleased( int button ){
    return !bit_is_clear(PINC, (PC1+ (button-1)));
}