#include <util/delay.h> 
#include <avr/io.h> 
#include <led.h> 

#define LED_PORT PORTB
#define LED_DDR DDRB
#define LED1 PB2
#define LED2 PB3
#define LED3 PB4
#define LED4 PB5



void enableLed  (uint8_t leds)
{
    LED_DDR = leds;  
}

void lightUpLed ( uint8_t led )    
{
       LED_PORT |= led;
}

void lightDownLed ( uint8_t led )
{       
       LED_PORT ^= led;
}


//  a function dimLed() to the led library that will dim the led during a certain period (in milliseconds) to a certain percentage.
void dimLeds (int percentage, int duration){
//enable the leds
    enableLeds();
    while (1){
         lightsUp();
        _delay_ms(percentage*duration/100); 
         lightsOff();
        _delay_ms(((100-percentage)*duration)/100);
    }
}
//enable all the leds
void enableLeds(){
LED_DDR= 0b00111100;
}
//disable the leds
void disableLeds(){
 LED_DDR= 0b00000000;   
}
//long duration light up
void longLightUp(){
        lightsUp();
        _delay_ms(2000);
}
//short duration light up
void shortLightUp(){
        lightsUp();
        _delay_ms(200);
}

void lightsUp(){
        LED_PORT &= ~_BV(LED1)&~_BV (LED2)&~_BV(LED3)&~_BV(LED4);
}
void lightsOff(){
        LED_PORT = _BV(LED1)|_BV (LED2)|_BV(LED3)|_BV(LED4); 
        _delay_ms(200);
}

void countdown(){
        enableLeds();
        //light up all 4 leds
        lightsUp();
        //after every second we turn off one led (counting down)
        _delay_ms(1000); 
        LED_PORT = _BV(LED4);
        _delay_ms(1000);
        LED_PORT |= _BV(LED3);
       _delay_ms(1000);
        LED_PORT |= _BV(LED2);
        _delay_ms(1000);
        LED_PORT |= _BV(LED1);
}

//initialize all leds and turn them off
void initLEDS(){
     LED_DDR |= _BV( LED4 )|_BV( LED3 )|_BV( LED2 )|_BV( LED1 );   
     LED_PORT |= _BV( LED4 )|_BV( LED3 )|_BV( LED2 )|_BV( LED1 );  
}

void enableOneLed( int led ){
if(led < 0 || led > 3) return;
        LED_DDR= _BV(PB2+led);
}

void lightDownOneLed( int led ){
if(led <0 || led > 3) return;        
        LED_PORT|= _BV(PB2+led);
}

void lightUpOneLed( int led ){
if(led <0 || led > 3) return;        
        LED_PORT&= ~_BV(PB2+led);
}


