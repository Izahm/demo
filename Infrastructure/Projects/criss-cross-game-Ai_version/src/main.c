#include <util/delay.h>
#include <string.h>
#include <usart.h>
#include <display.h>
#include <stdlib.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <potentio.h>
#include <button.h>
#include <buzzer.h>
#include <led.h>


//solfege 
#define C5  523.250
#define D5  587.330
#define E5  659.250
#define F5  698.460
#define G5  783.990
#define A5  880.00
#define B5  987.770
#define C6  1046.500
#define C  261
#define D  294
#define E  329
#define F  349
#define G  391
#define GS 415
#define A  440
#define AS 455
#define B  466
#define CH  523
#define CSH  554
#define DH  587
#define DSH  622
#define EH  659
#define FH  698
#define FSH  740
#define GH  784
#define GSH  830
#define AH  880

#define DURATION 100

#define BUTTON_PORT PORTC
#define BUTTON_PIN PINC
#define BUTTON_DDR DDRC
#define BUTTON1 PC1
#define BUTTON2 PC2
#define BUTTON3 PC3


#define GAME_OVER (playNum == 20)
#define MAX_RANGE 9

#define END (end==1 || end ==-1)


int playNum = MAX_RANGE;

//Global variables
int seed; //seeder

//rounds mem loc
int* humanRounds;
int* NPCRounds;

//game players
char players[]={'X','O'}; 

int playerTurn=0; //changes through out the game

//Number of played rounds
int playCounter=0;
int roundCounter=0;

//end game 
int winner;

int end;


ISR( PCINT1_vect )
{
    // is any button clicked?
    if ( isAnyClicked())
    {
      //debounce
        _delay_us( 1000 );
        
        if ( bit_is_clear( BUTTON_PIN, BUTTON1 ))
        {          
               
               decplayNum();   
        }
        else if ( bit_is_clear( BUTTON_PIN, BUTTON2 ))
        {      
               playTurn();   
               setUpTurn(); 
               aiNextMove(playerTurn);
        }
        else if ( bit_is_clear( BUTTON_PIN, BUTTON3 ))
        {          
               incplayNum();
        }
    }
}

int main()
{

  initDisplay();
  initADC();
  initUSART();
  initBTN();
  enableBTNinterrupt();
  initLEDS();

/* char movie1[] = "Something";



    char * movie2 = &movie1;
    *(movie2)='A';
     
    printf("str: %s", movie1); */

    int i, howMany;
int total=0;
float average = 0.0;
int  * gradesArray;

printf("How many numbers do you want to average?");
scanf(" %d", &howMany);
gradesArray = malloc(sizeof(int)*howMany);

printf("Enter your grades");

for( i; i< howMany; i++){
scanf("%d", &gradesArray[i]);
total+= gradesArray[i];
}

    return 0;
}


//Game play
void incplayNum(){
playNum++;
    if(playNum==(MAX_RANGE+1)){
    playNum=1;
    }
}

void decplayNum(){
playNum--;
    if(playNum==0){
         playNum=MAX_RANGE;
    }
}

char board[10] = { 'O', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
void playTurn()
{
    enableBuzzer();

    int player = 1, i;  
    char mark;
    int play=0;   
          //shorth&& if-else
         player = playerTurn==0 ? 1 : 2;

        mark = (player == 1) ? 'X' : 'O';
        if (playNum == 1 && board[1] == '1'){
            board[1] = mark;
            play=1;
            }
        else if (playNum == 2 && board[2] == '2'){
            board[2] = mark;
            play=1;
            }
        else if (playNum == 3 && board[3] == '3'){
            board[3] = mark;
            play=1;
            }
        else if (playNum == 4 && board[4] == '4'){
            board[4] = mark;
            play=1;
            }
        else if (playNum == 5 && board[5] == '5'){
            board[5] = mark;
            play=1;
            }
        else if (playNum == 6 && board[6] == '6'){
            board[6] = mark;
            play=1;
            }
        else if (playNum == 7 && board[7] == '7'){
            board[7] = mark;
            play=1;
            }
        else if (playNum == 8 && board[8] == '8'){
            board[8] = mark;
            play=1;
            }
        else if (playNum == 9 && board[9] == '9'){
            board[9] = mark;
        play=1;
            }
        else {//if player tried to pick a checked board
          lightsUp();
          playTone(120,350);
          lightsOff();
          setUpTurn();//replay the turn
        }
        if(play==1) //if the player picks a valid board
         playTone(G,DURATION);
        i= checkwin();
        printBoard();
    
    //if there is a winner
    if (i == 1){
       printf("==>    Player %d wins ", player);
       //indicate that there is a winner && game is over 
       end=1;
       winner = player-1;
       }
    else if(i==-1){
       printf("==>    Game is still going");
    }
    else{
       printf("==>    Game draw");
       end=-1;
    }
}

int checkwin()
{
    // A player crosses a row (wins)  
    if (board[1] == board[2] && board[2] == board[3])
        return 1;
        
    else if (board[4] == board[5] && board[5] == board[6])
        return 1;
        
    else if (board[7] == board[8] && board[8] == board[9])
        return 1;
        
    else if (board[1] == board[4] && board[4] == board[7])
        return 1;
        
    else if (board[2] == board[5] && board[5] == board[8])
        return 1;
        
    else if (board[3] == board[6] && board[6] == board[9])
        return 1;
        
    else if (board[1] == board[5] && board[5] == board[9])
        return 1;
        
    else if (board[3] == board[5] && board[5] == board[7])
        return 1;
       
       //Draw (if all the boardes are checked with no winner)
    else if (board[1] != '1' && board[2] != '2' && board[3] != '3' &&
        board[4] != '4' && board[5] != '5' && board[6] != '6' && board[7] 
        != '7' && board[8] != '8' && board[9] != '9'){
        return 0;}

     //Game still going (if none of the conditions above are met)
    else{
        return  -1;}
}


//Logic
void setUpTurn(){
    playerTurn++;
    if(playerTurn==2){
        playerTurn=0;
    }
}

void randomTurn()
{
    srand(seed);
    int num = (rand() % 2);
    playerTurn = num;
}

void aiNextMove(int currentTurn){
//preping for next turn
//check for AI turn
if(currentTurn==1)
   { 
     if (board[5] != 'O' && board[5] != 'X')
         playNum= 5;
    else if(board[1] == 'O' && board[2] == 'O' && board[3] != 'O' && board[3] != 'X')
         playNum= 3;
    else if(board[2] == 'O' && board[3] == 'O' && board[1] != 'O' && board[1] != 'X')
         playNum=1;
      else if(board[1] == 'O' && board[3] == 'O' && board[2] != 'O' && board[2] != 'X')
         playNum= 2;
    else if(board[4] == 'O' && board[5] == 'O' && board[6] != 'O' && board[6] != 'X')
         playNum= 6;
    else if(board[4] == 'O' && board[6] == 'O' && board[5] != 'O' && board[5] != 'X')
         playNum= 5;
    else if(board[6] == 'O' && board[5] == 'O' && board[4] != 'O' && board[4] != 'X')
         playNum= 4;
    else if(board[7] == 'O' && board[8] == 'O' && board[9] != 'O' && board[9] != 'X')
         playNum= 9;
      else if(board[7] == 'O' && board[9] == 'O' && board[8] != 'O' && board[8] != 'X')
         playNum= 8;
    else if(board[8] == 'O' && board[9] == 'O' && board[7] != 'O' && board[7] != 'X')
         playNum= 7;
    else if(board[1] == 'O' && board[4] == 'O' && board[7] != 'O' && board[7] != 'X')
         playNum= 7;
      else if(board[1] == 'O' && board[7] == 'O' && board[4] != 'O' && board[4] != 'X')
         playNum= 4;
    else if(board[4] == 'O' && board[7] == 'O' && board[1] != 'O' && board[1] != 'X')
         playNum= 7;
    else if(board[2] == 'O' && board[5] == 'O' && board[8] != 'O' && board[8] != 'X')
         playNum= 8;
      else if(board[2] == 'O' && board[8] == 'O' && board[5] != 'O' && board[5] != 'X')
         playNum= 5;
    else if(board[5] == 'O' && board[8] == 'O' && board[2] != 'O' && board[2] != 'X')
         playNum= 2;
    else if(board[3] == 'O' && board[6] == 'O' && board[9] != 'O' && board[9] != 'X')
         playNum= 9;
      else if(board[3] == 'O' && board[9] == 'O' && board[6] != 'O' && board[6] != 'X')
         playNum= 5;
    else if(board[6] == 'O' && board[9] == 'O' && board[3] != 'O' && board[3] != 'X')
         playNum= 3;
    else if(board[3] == 'O' && board[5] == 'O' && board[7] != 'O' && board[7] != 'X')
         playNum= 7;
      else if(board[5] == 'O' && board[7] == 'O' && board[3] != 'O' && board[3] != 'X')
         playNum= 3;
    else if(board[3] == 'O' && board[7] == 'O' && board[5] != 'O' && board[5] != 'X')
         playNum= 5;
    else if(board[1] == 'O' && board[5] == 'O' && board[9] != 'O' && board[9] != 'X')
         playNum= 9;
      else if(board[1] == 'O' && board[9] == 'O' && board[5] != 'O' && board[5] != 'X')
         playNum= 5;
    else if(board[5] == 'O' && board[9] == 'O' && board[1] != 'O' && board[5] != 'X')
         playNum= 1;
// to prevent human from winning
     else if (board[1] == 'X' && board[2] == 'X' && board[3] != 'X' && board[3] != 'O')
         playNum= 3;
     else if (board[2] == 'X' && board[3] == 'X' && board[1] != 'X' && board[1] != 'O')
         playNum= 1;
       else if (board[1] == 'X' && board[3] == 'X' && board[2] != 'X' && board[2] != 'O')
         playNum= 2;
     else if (board[4] == 'X' && board[5] == 'X' && board[6] != 'X' && board[6] != 'O')
         playNum= 6;
     else if (board[4] == 'X' && board[6] == 'X' && board[5] != 'X' && board[5] != 'O')
         playNum= 5;
     else if (board[6] == 'X' && board[5] == 'X' && board[4] != 'X' && board[4] != 'O')
         playNum= 4;
     else if (board[7] == 'X' && board[8] == 'X' && board[9] != 'X' && board[9] != 'O')
         playNum= 9;
       else if (board[7] == 'X' && board[9] == 'X' && board[8] != 'X' && board[8] != 'O')
         playNum= 8;
     else if (board[8] == 'X' && board[9] == 'X' && board[7] != 'X' && board[7] != 'O')
         playNum= 7;
     else if (board[1] == 'X' && board[4] == 'X' && board[7] != 'X' && board[7] != 'O')
         playNum= 7;
       else if (board[1] == 'X' && board[7] == 'X' && board[4] != 'X' && board[4] != 'O')
         playNum= 4;
     else if (board[4] == 'X' && board[7] == 'X' && board[1] != 'X' && board[1] != 'O')
         playNum= 7;
     else if (board[2] == 'X' && board[5] == 'X' && board[8] != 'X' && board[8] != 'O')
         playNum= 8;
       else if (board[2] == 'X' && board[8] == 'X' && board[5] != 'X' && board[5] != 'O')
         playNum= 5;
      else if (board[5] == 'X' && board[8] == 'X' && board[2] != 'X' && board[2] != 'O')
          playNum= 2;
      else if (board[3] == 'X' && board[6] == 'X' && board[9] != 'X' && board[9] != 'O')
         playNum= 9;
       else if (board[3] == 'X' && board[9] == 'X' && board[6] != 'X' && board[6] != 'O')
         playNum= 5;
       else if (board[6] == 'X' && board[9] == 'X' && board[3] != 'X' && board[3] != 'O')
         playNum= 3;
       else if (board[3] == 'X' && board[5] == 'X' && board[7] != 'X' && board[7] != 'O')
         playNum= 7;
       else if (board[5] == 'X' && board[7] == 'X' && board[3] != 'X' && board[3] != 'O')
         playNum= 3;
       else if (board[3] == 'X' && board[7] == 'X' && board[5] != 'X' && board[5] != 'O')
         playNum= 5;
       else if (board[1] == 'X' && board[5] == 'X' && board[9] != 'X' && board[9] != 'O')
         playNum= 9;
       else if (board[1] == 'X' && board[9] == 'X' && board[5] != 'X' && board[5] != 'O')
         playNum= 5;
       else if (board[5] == 'X' && board[9] == 'X' && board[1] != 'X' && board[5] != 'O')
         playNum= 1;
         else {
             int seed = rand() % 1000;
             srand(seed);
             playNum = (rand()% 8)+1;
         } 
   } 
}

//Displays
void displayplayNum(){
    writeNumberToSegment(0, playNum);
}

void displayPlayer(){
    writeCharToSegment(2,'P');
    writeNumberToSegment(3,playerTurn+1);
    
}

void displayEnd(int end){
  if(end==1){
  char message[] = "WIN";
  char player = players[winner];
  strncat(message, &player, 1);
  writeStringAndWait(message,1000);
  return;
  }
  else{
    writeString("DRAW");
  }
 
}

void printBoard()
{
    printf("\n\n\tTic Tac Toe\n\n");
    printf("Player 1 (X)  -  Player 2 (O)\n\n\n");
    printf("         |     |     \n");
    printf("      %c  |  %c  |  %c \n", board[1], board[2], board[3]);
    printf("    _____|_____|_____\n");
    printf("         |     |     \n");
    printf("      %c  |  %c  |  %c \n", board[4], board[5], board[6]);
    printf("    _____|_____|_____\n");
    printf("         |     |     \n");
    printf("      %c  |  %c  |  %c \n",board[7], board[8], board[9]);
    printf("         |     |     \n\n");
}

//Sounds
void playIntroTrack(){
          displayPlayer();
          firstSection();
          secondSection();
}

void firstSection()
{
  playTone(A, 500);
  playTone(A, 500);    
  playTone(A, 500);
  playTone(F, 350);
  playTone(CH, 150);  
  playTone(A, 500);
  playTone(F, 350);
  playTone(CH, 150);
  playTone(A, 650);
 
  _delay_ms(500);
 
  playTone(EH, 500);
  playTone(EH, 500);
  playTone(EH, 500);  
  playTone(FH, 350);
  playTone(CH, 150);
  playTone(GS, 500);
  playTone(F, 350);
  playTone(CH, 150);
  playTone(A, 650);
 
  _delay_ms(500);
}
 
void secondSection()
{
  playTone(AH, 500);
  playTone(A, 300);
  playTone(A, 150);
  playTone(AH, 500);
  playTone(GSH, 325);
  playTone(GH, 175);
  playTone(FSH, 125);
  playTone(FH, 125);    
  playTone(FSH, 250);
 
  _delay_ms(325);
 
  playTone(AS, 250);
  playTone(DSH, 500);
  playTone(DH, 325);  
  playTone(CSH, 175);  
  playTone(CH, 125);  
  playTone(B, 125);  
  playTone(CH, 250);  
 
  _delay_ms(350);
} 

