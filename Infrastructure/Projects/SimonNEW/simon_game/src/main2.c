/* #include <util/delay.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <LEDLib.h>
#include <ButtonLibrary.h>
#include <usart.h>

#define LED_PORT PORTB
#define BUTTON_PORT PORTC
#define BUTTON_PIN PINC
#define LED_DDR DDRB
#define BUTTON_DDR DDRC
#define BUTTON1 PC1
#define BUTTON2 PC2
#define BUTTON3 PC3
#define LED1 PB2
#define LED2 PB3
#define LED3 PB4
#define LED4 PB5
#define DELAY 500
#define NUMBER_OF_BUTTONS 3


int button_pushed = 0;
int playerLost = 0; //UserFailed
int playerChoice = 0;

void LEDEnab(){
    for ( int i = 0; i < 4; i++ )
  {
    enableLed(i);
  }
  allLEDOff();
}

void LEDLoading(){
  
}

void LEDWIN(){
  allLEDOn();
  _delay_ms(250);
  allLEDOff();
  _delay_ms(250);
  allLEDOn();
  _delay_ms(250);
  allLEDOff();
}

void LEDLOSE(){
  allLEDOn();
  _delay_ms(250);
  allLEDOff();

}

ISR(PCINT1_vect)
{
  if (bit_is_clear(BUTTON_PIN, BUTTON1))
  {
    button_pushed = 1;
  }
}

void generatePuzzle(uint8_t array[], int arrayLenght){
  for (int i = 0; i < arrayLenght; i++)
  {
    array[i] = rand()%3;
  }
}

void printPuzzle(uint8_t array[], int arrayLenght){
  for (int i = 0; i < arrayLenght; i++)
  {
    printf("%d ", array[i]);
  }
}

int findPiece(uint8_t array[], int index) {
  return array[index];
}

void playPuzzle(uint8_t array[], int arrayLenght)
{
  for (int i = 0; i < arrayLenght; i++)
  {
    lightUpLed(array[i]);
    _delay_ms(DELAY);
    lightDownLed(array[i]);
    _delay_ms(200);
  }
}

void readInput(uint8_t array[], int arrayEntry)
{
  button_pushed = 0;
  while (button_pushed == 0)
  {
    if (bit_is_clear(PINC, PC1))
    {
      lightUpLed(0);
      _delay_ms(100);
      lightDownLed(0);
      _delay_ms(300);
      button_pushed = 1;
      playerChoice = 0;
    }
    else if (bit_is_clear(PINC, PC2))
    {
      lightUpLed(1);
      _delay_ms(100);
      lightDownLed(1);
      _delay_ms(300);
      button_pushed = 1;
      playerChoice = 1;
    }
    else if (bit_is_clear(PINC, PC3))
    {
      lightUpLed(2);
      _delay_ms(100);
      lightDownLed(2);
      _delay_ms(300);
      button_pushed = 1;
      playerChoice = 2;
    }
  }
  if(playerChoice == array[arrayEntry]){
    printf("Correct! \n");
  }else{
    printf("Wrong!\n");
    playerLost = 1;
  }
}

int main(){
  initUSART();
  LEDEnab();

  LED_DDR |= _BV( LED1 ) | _BV( LED2 ) | _BV( LED3 ) | _BV( LED4 );   
  LED_PORT |= _BV( LED1 ) | _BV( LED2 ) | _BV( LED3 ) | _BV( LED4 );  
  BUTTON_DDR &= ~_BV( BUTTON1 );
  BUTTON_DDR &= ~_BV( BUTTON2 ); 
  BUTTON_DDR &= ~_BV( BUTTON3 );           
  BUTTON_PORT |= _BV( BUTTON1 ) | _BV( BUTTON2 ) | _BV( BUTTON3 );

  PCICR |= _BV( PCIE1 ); 

  PCMSK1 |= _BV( BUTTON1 ); 

  sei(); 
  int seed = 0;
  while (button_pushed == 0)
  {
    lightUpLed(2);
    _delay_ms(75);
    lightDownLed(2);
    _delay_ms(75);
    seed++;
  }
  
  srand(seed);

  uint8_t randnumb[10];

  PCMSK1 |= 0;

  generatePuzzle(randnumb, 10);
  printPuzzle(randnumb, 10);
  for (int i = 1; i <= 10; i++)
  {
    if (playerLost == 0)
    {
      playPuzzle(randnumb, i);
      for (int j = 0; j < i; j++)
      {
        readInput(randnumb, j);
      }
    }
  }
  if(playerLost == 0){
    printf("You are the Simon Says master!");
    LEDWIN();
  } else {
    LEDLOSE();
  }
  return 0;
}
 */