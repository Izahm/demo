#include <util/delay.h>
#include <string.h>
#include <usart.h>
#include <display.h>
#include <stdlib.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <potentio.h>
#include <button.h>
#include <buzzer.h>
#include <led.h>

//solfege
#define C5 523.250
#define D5 587.330
#define E5 659.250
#define F5 698.460
#define G5 783.990
#define A5 880.00
#define B5 987.770
#define C6 1046.500
#define C 261
#define D 294
#define E 329
#define F 349
#define G 391
#define GS 415
#define A 440
#define AS 455
#define B 466
#define CH 523
#define CSH 554
#define DH 587
#define DSH 622
#define EH 659
#define FH 698
#define FSH 740
#define GH 784
#define GSH 830
#define AH 880

#define DURATION 100



#define COMPUTER 1
#define HUMAN 0
#define MAX_COL 3 
#define MAX_ROW 3
#define END (end == 1 || end == -1)

//Global variables
//----------------------------------------------------------//
int playNum = 1; // playable col numbers


int npcRounds, humanRounds;
int *humanMoves, *NPCMoves;


//game players
char players[] = {'X', 'C'};
int playerTurn; //changes through out the game
int currentTurn;

//Number of played rounds
int playCounter = 0;
int roundCounter = 0;

//end game
//char winner;
int winner;
int end;

//Board
char board[10] = {'o', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
int row = 0; //initial row for display

void memAlloc();
void freeMemory();
void incrementInt();
void incrColNum();
void playTurn();
int checkLine();
int checkwin();
void setUpTurn();
int randomTurn();
void displayplayNum();
void displayEnd();
void scroll();
void displayBoard();
void printBoard();
void playTrack();
void aiNextMove();
int sum();



//----------------------------------------------------------//
ISR(PCINT1_vect)
{
    // is any button clicked?
    if (isAnyClicked())
    {
        //debounce
        _delay_us(1000);

        if (buttonPushed(1))
        {
            scroll(); //Scroll the screen
        }
        else if (buttonPushed(2))
        {
            playTurn(); //Pl
            setUpTurn();
            if(playerTurn==COMPUTER)
                aiNextMove(board);
             //Please check the end of the code to find the rules implemented for AI
        }
        else if (buttonPushed(3))
        {
            incrColNum();
        }
    }
}

int main()
{
    memAlloc();
    initLEDS();
    initDisplay();
    initADC();
    initUSART();
    initBTN();
    enableBTNinterrupt();
    initLEDS();
    sei();
    while (1)
    {
        initADC();
        startADC();
        int seed;//seeder
        uint16_t value = readADCvalue();
        seed = value;
        writeNumberAndWait(seed, 500);
        if (buttonPushed(3))
        {
            playerTurn = randomTurn(&seed);
            if(playerTurn==COMPUTER)
                aiNextMove(playerTurn);
            printBoard();
            while (1)
            {
                if (END)
                {
                    playTrack();
                    displayEnd(end);
                    break;
                }
                else
                {
                    lightUpOneLed(currentTurn);
                    displayBoard(row, board);
                    displayplayNum();
                }
            }
        }
    }
    return 0;
}



//Memory managment
void memAlloc(){
humanMoves = (int *) malloc(sizeof(int));
NPCMoves =(int *) malloc(sizeof(int));
}
void freeMemory(){
    free(humanMoves);
    free(NPCMoves);
}


//Utility
void incrementInt(int *num)
{
    (*num)++;
}

int sum(int * arr, int size){
            int sum =0;
            for(int i =0; i<size; i++)
              {
            sum +=  arr[i];
            }
     return sum;
}


//Game play
void incrColNum()
{
    playNum++;
    if (playNum == (MAX_COL + 1))
    {
        playNum = 1;
    }
}

void playTurn()
{
    enableBuzzer();
    //temp variable Player that ended the game (player, one position before next)

    int player = 1, i, sumOfMoves;
    char mark;
    int valid;
    //shorthand if-else
    player = playerTurn == HUMAN ? 1 : 2;
    //mark = (player == 1) ? players[0].display : players[1].display;
    mark = (player == 1) ? 'X' : 'O';
    int led = (mark == 'X') ? 1 : 3;

    if (row == 0 && playNum == 1 && board[1] == '1')
    {
        board[1] = mark;
        valid = 1;
    }
    else if (row == 0 && playNum == 2 && board[2] == '2')
    {
        board[2] = mark;
        valid = 1;
    }
    else if (row == 0 && playNum == 3 && board[3] == '3')
    {
        board[3] = mark;
        valid = 1;
    }
    else if (row == 1 && playNum == 1 && board[4] == '4')
    {
        board[4] = mark;
        valid = 1;
    }
    else if (row == 1 && playNum == 2 && board[5] == '5')
    {
        board[5] = mark;
        valid = 1;
    }
    else if (row == 1 && playNum == 3 && board[6] == '6')
    {
        board[6] = mark;
        valid = 1;
    }
    else if (row == 2 && playNum == 1 && board[7] == '7')
    {
        board[7] = mark;
        valid = 1;
    }
    else if (row == 2 && playNum == 2 && board[8] == '8')
    {
        board[8] = mark;
        valid = 1;
    }
    else if (row == 2 && playNum == 3 && board[9] == '9')
    {
        board[9] = mark;
        valid = 1;
    }
    else
    { //if player tried to pick a checked board
        lightsUp();
        playTone(120, 350);
        lightsOff();
        setUpTurn(); //replay the turn
    }
    if (valid == 1) //if the player picks a valid board
        {
            if(playerTurn==COMPUTER){
                NPCMoves[npcRounds]=1;
                npcRounds++;
            }
            else {
                humanMoves[humanRounds]=1;
                humanRounds++;
            }
        playTone(G, DURATION);
        }
        
    //check if the game ended?win:draow
    i = checkwin();
    printBoard();

    //if there is a winner
    if (i == 1)
    {
        //winner = players[playerTurn].display;
        winner = playerTurn;
        if (playerTurn == COMPUTER)
        {
            printf("\nComputer wins with a total number moves of %d !\nThat's a pitty, You lost to an NPC :(",sum(NPCMoves,npcRounds));
            freeMemory();
        }
        else
        {
            printf("\nYou won with a total number moves of %d !", HUMAN, sum(humanMoves,humanRounds));
            freeMemory();
        }
        //indicate that there is a winner and game is over
        end = 1;
    }
    else if (i == -1)
    {
        //reset playable column to 1 after a round is played
        playNum = 1;
    }
    else
    {
        printf("\nDraw!");
        end = -1;
    }
}

int checkLine(int pos1, int pos2, int pos3)
{
    if (board[pos1] == board[pos2] && board[pos2] == board[pos3])
        return 1;
}

int checkwin()
{
    // A player crosses a row (wins)
    if (checkLine(1, 2, 3) == 1)
        return 1;

    else if (checkLine(4, 5, 6) == 1)
        return 1;

    else if (checkLine(7, 8, 9) == 1)
        return 1;

    else if (checkLine(1, 4, 7) == 1)
        return 1;

    else if (checkLine(2, 5, 8) == 1)
        return 1;

    else if (checkLine(3, 6, 9) == 1)
        return 1;

    else if (checkLine(1, 5, 9) == 1)
        return 1;

    else if (checkLine(3, 5, 7) == 1)
        return 1;

    //Draw (if all the boardes are checked with no winner)
    else if (board[1] != '1' && board[2] != '2' && board[3] != '3' &&
             board[4] != '4' && board[5] != '5' && board[6] != '6' && board[7] != '7' && board[8] != '8' && board[9] != '9')
    {
        return 0;
    }

    //Game still going (if none of the conditions above are met)
    else
    {
        return -1;
    }
}

//Logic
void setUpTurn()
{
    lightDownOneLed(currentTurn);
    currentTurn = playerTurn;
    playerTurn++;
    if (playerTurn == 2)
    {
        playerTurn = 0;
    }
}

int randomTurn(int * seed)
{
    srand(*(seed));
    int num = (rand() % 2);
    return num;
}
//Please find the AI futherdown



//LC-Displays
void displayplayNum()
{
    writeNumberToSegment(3, playNum);
    lightUpDecimal(playNum - 1);
}

//Display the winner
void displayEnd(int end)
{
    if (end == 1)
    {
        char message[] = "WNR";
        char player = players[winner];
        strncat(message, &player, 1);
        writeStringAndWait(message, 2500);
        return;
    }
    else
    {
        writeString("DRAW");
    }
}
//Scroll through the board rows (to-be displayed on LCD)
void scroll()
{
    enableBuzzer();
    playTone(700, DURATION);
    incrementInt(&row);
    if (row == 3)
    {
        row = 0;
    }
    displayBoard(row, board);
}

//Display the board rows on LCD screen 
void displayBoard(int row, char board[])
{
    //First create a copy of the original board
    char board_parsed[10];
    for (int i = 0; i < 10; i++)
    {
        board_parsed[i] = board[i];
    }
    //Parse the boardCopy
    for (int i = 1; i < 10; i++)
    {
        if (board_parsed[i] != 'X' && board_parsed[i] != 'O')
        {
            board_parsed[i] = board_parsed[i] - '0'; 
        }
    }
    switch (row)
    {
    case 0:
        writeCharToSegment(0, board_parsed[1]);
        writeCharToSegment(1, board_parsed[2]);
        writeCharToSegment(2, board_parsed[3]);
        break;
    case 1:
        writeCharToSegment(0, board_parsed[4]);
        writeCharToSegment(1, board_parsed[5]);
        writeCharToSegment(2, board_parsed[6]);
        break;
    case 2:
        writeCharToSegment(0, board_parsed[7]);
        writeCharToSegment(1, board_parsed[8]);
        writeCharToSegment(2, board_parsed[9]);
        break;
    default:
        break;
    }
}

//Serial monitor
void printBoard()
{
    printf("\n\n\tTic Tac Toe\n");
    printf("\nCurrent player's turn %c\n", players[currentTurn]);
    printf(" Human (X)  -  AI (O)\n\n");
    printf("         |     |     \n");
    printf("      %c  |  %c  |  %c \n", board[1], board[2], board[3]);
    printf("    _____|_____|_____\n");
    printf("         |     |     \n");
    printf("      %c  |  %c  |  %c \n", board[4], board[5], board[6]);
    printf("    _____|_____|_____\n");
    printf("         |     |     \n");
    printf("      %c  |  %c  |  %c \n", board[7], board[8], board[9]);
    printf("         |     |     \n\n");
}

//Sounds
uint32_t frequencies[] = {C5, D5, E5, F5, G5, A5, B5, C6, A, F, CH, EH, FH, GS};
uint8_t durations[] = {1000, 500, 250};

void playTrack()
{
    playTone(frequencies[8], 500);
    playTone(frequencies[8], 500);
    playTone(frequencies[8], 500);
    playTone(frequencies[9], 350);
    playTone(frequencies[10], 150);
    playTone(frequencies[8], 500);
    playTone(frequencies[9], 350);
    playTone(frequencies[10], 150);
    playTone(frequencies[8], 650);
    _delay_ms(500);
    playTone(frequencies[11], 500);
    playTone(frequencies[11], 500);
    playTone(frequencies[11], 500);
    playTone(frequencies[12], 350);
    playTone(frequencies[10], 150);
    playTone(frequencies[13], 500);
    playTone(frequencies[9], 350);
    playTone(frequencies[10], 150);
    playTone(frequencies[8], 650);
    _delay_ms(500);
}

//AI
void aiNextMove(char board[])
{   //preping for next turn
    //check for AI turn
    {
        if (board[5] != 'O' && board[5] != 'X')
        {
            playNum = 2;
            row = 1;
        }
        else if (board[1] == 'O' && board[2] == 'O' && board[3] != 'O' && board[3] != 'X')
        {
            playNum = 3;
            row = 0;
        }
        else if (board[2] == 'O' && board[3] == 'O' && board[1] != 'O' && board[1] != 'X')
        {
            playNum = 1;
            row = 0;
        }
        else if (board[1] == 'O' && board[3] == 'O' && board[2] != 'O' && board[2] != 'X')
        {
            playNum = 2;
            row = 0;
        }
        else if (board[4] == 'O' && board[5] == 'O' && board[6] != 'O' && board[6] != 'X')
        {
            playNum = 3;
            row = 1;
        }
        else if (board[3] == 'O' && board[9] == 'O' && board[6] != 'O' && board[6] != 'X')
        {
            playNum = 3;
            row = 1;
        }
        else if (board[4] == 'O' && board[6] == 'O' && board[5] != 'O' && board[5] != 'X')
        {
            playNum = 2;
            row = 1;
        }
        else if (board[6] == 'O' && board[5] == 'O' && board[4] != 'O' && board[4] != 'X')
        {
            playNum = 1;
            row = 1;
        }
        else if (board[7] == 'O' && board[8] == 'O' && board[9] != 'O' && board[9] != 'X')
        {
            playNum = 3;
            row = 2;
        }
        else if (board[7] == 'O' && board[9] == 'O' && board[8] != 'O' && board[8] != 'X')
        {
            playNum = 2;
            row = 2;
        }
        else if (board[8] == 'O' && board[9] == 'O' && board[7] != 'O' && board[7] != 'X')
        {
            playNum = 1;
            row = 2;
        }
        else if (board[1] == 'O' && board[4] == 'O' && board[7] != 'O' && board[7] != 'X')
        {
            playNum = 1;
            row = 2;
        }
        else if (board[1] == 'O' && board[7] == 'O' && board[4] != 'O' && board[4] != 'X')
        {
            playNum = 1;
            row = 1;
        }
        else if (board[4] == 'O' && board[7] == 'O' && board[1] != 'O' && board[1] != 'X')
        {
            playNum = 1;
            row = 2;
        }
        else if (board[2] == 'O' && board[5] == 'O' && board[8] != 'O' && board[8] != 'X')
        {
            playNum = 2;
            row = 2;
        }
        else if (board[2] == 'O' && board[8] == 'O' && board[5] != 'O' && board[5] != 'X')
        {
            playNum = 2;
            row = 1;
        }
        else if (board[5] == 'O' && board[8] == 'O' && board[2] != 'O' && board[2] != 'X')
        {
            playNum = 2;
            row = 0;
        }
        else if (board[3] == 'O' && board[6] == 'O' && board[9] != 'O' && board[9] != 'X')
        {
            playNum = 3;
            row = 2;
        }
        else if (board[3] == 'O' && board[9] == 'O' && board[6] != 'O' && board[6] != 'X')
        {
            playNum = 2;
            row = 1;
        }
        else if (board[6] == 'O' && board[9] == 'O' && board[3] != 'O' && board[3] != 'X')
        {
            playNum = 3;
            row = 0;
        }
        else if (board[3] == 'O' && board[5] == 'O' && board[7] != 'O' && board[7] != 'X')
        {
            playNum = 1;
            row = 2;
        }
        else if (board[5] == 'O' && board[7] == 'O' && board[3] != 'O' && board[3] != 'X')
        {
            playNum = 3;
            row = 0;
        }
        else if (board[3] == 'O' && board[7] == 'O' && board[5] != 'O' && board[5] != 'X')
        {
            playNum = 2;
            row = 1;
        }
        else if (board[1] == 'O' && board[5] == 'O' && board[9] != 'O' && board[9] != 'X')
        {
            playNum = 3;
            row = 2;
        }
        else if (board[1] == 'O' && board[9] == 'O' && board[5] != 'O' && board[5] != 'X')
        {
            playNum = 2;
            row = 1;
        }
        else if (board[5] == 'O' && board[9] == 'O' && board[1] != 'O' && board[5] != 'X')
        {
            playNum = 1;
            row = 0;
        }
        // to prevent human from winning
        else if (board[1] == 'X' && board[2] == 'X' && board[3] != 'X' && board[3] != 'O')
        {
            playNum = 3;
            row = 0;
        }
        else if (board[2] == 'X' && board[3] == 'X' && board[1] != 'X' && board[1] != 'O')
        {
            playNum = 1;
            row = 0;
        }
        else if (board[1] == 'X' && board[3] == 'X' && board[2] != 'X' && board[2] != 'O')
        {
            playNum = 2;
            row = 0;
        }
        else if (board[4] == 'X' && board[5] == 'X' && board[6] != 'X' && board[6] != 'O')
        {
            playNum = 3;
            row = 1;
        }
        else if (board[3] == 'X' && board[9] == 'X' && board[6] != 'X' && board[6] != 'O')
        {
            playNum = 3;
            row = 1;
        }
        else if (board[4] == 'X' && board[6] == 'X' && board[5] != 'X' && board[5] != 'O')
        {
            playNum = 2;
            row = 1;
        }
        else if (board[6] == 'X' && board[5] == 'X' && board[4] != 'X' && board[4] != 'O')
        {
            playNum = 4;
            row = 1;
        }
        else if (board[7] == 'X' && board[8] == 'X' && board[9] != 'X' && board[9] != 'O')
        {
            playNum = 3;
            row = 2;
        }
        else if (board[7] == 'X' && board[9] == 'X' && board[8] != 'X' && board[8] != 'O')
        {
            playNum = 2;
            row = 2;
        }
        else if (board[8] == 'X' && board[9] == 'X' && board[7] != 'X' && board[7] != 'O')
        {
            playNum = 1;
            row = 2;
        }
        else if (board[1] == 'X' && board[4] == 'X' && board[7] != 'X' && board[7] != 'O')
        {
            playNum = 1;
            row = 2;
        }
        else if (board[1] == 'X' && board[7] == 'X' && board[4] != 'X' && board[4] != 'O')
        {
            playNum = 1;
            row = 1;
        }
        else if (board[4] == 'X' && board[7] == 'X' && board[1] != 'X' && board[1] != 'O')
        {
            playNum = 1;
            row = 2;
        }
        else if (board[2] == 'X' && board[5] == 'X' && board[8] != 'X' && board[8] != 'O')
        {
            playNum = 2;
            row = 2;
        }
        else if (board[2] == 'X' && board[8] == 'X' && board[5] != 'X' && board[5] != 'O')
        {
            playNum = 2;
            row = 1;
        }
        else if (board[5] == 'X' && board[8] == 'X' && board[2] != 'X' && board[2] != 'O')
        {
            playNum = 2;
            row = 0;
        }
        else if (board[3] == 'X' && board[6] == 'X' && board[9] != 'X' && board[9] != 'O')
        {
            playNum = 3;
            row = 2;
        }
        else if (board[3] == 'X' && board[9] == 'X' && board[6] != 'X' && board[6] != 'O')
        {
            playNum = 2;
            row = 1;
        }
        else if (board[6] == 'X' && board[9] == 'X' && board[3] != 'X' && board[3] != 'O')
        {
            playNum = 3;
            row = 0;
        }
        else if (board[3] == 'X' && board[5] == 'X' && board[7] != 'X' && board[7] != 'O')
        {
            playNum = 1;
            row = 2;
        }
        else if (board[5] == 'X' && board[7] == 'X' && board[3] != 'X' && board[3] != 'O')
        {
            playNum = 3;
            row = 0;
        }
        else if (board[3] == 'X' && board[7] == 'X' && board[5] != 'X' && board[5] != 'O')
        {
            playNum = 2;
            row = 1;
        }
        else if (board[1] == 'X' && board[5] == 'X' && board[9] != 'X' && board[9] != 'O')
        {
            playNum = 3;
            row = 2;
        }
        else if (board[1] == 'X' && board[9] == 'X' && board[5] != 'X' && board[5] != 'O')
        {
            playNum = 2;
            row = 1;
        }
        else if (board[5] == 'X' && board[9] == 'X' && board[1] != 'X' && board[5] != 'O')
        {
            playNum = 1;
            row = 0;
        }
        else
        { //If none of the conditions above is met, then pick an empty col
         for (int b_row =0; b_row <3; b_row ++){
             for (int b_col=1; b_col<=3; b_col++){
             if(board[b_col]!='X' && board[b_col]!= 'O'){
                 playNum=b_col;
                 row = b_row;
             }
             }
         }
        
       /*  This code below looks for a random empty col inside a do while loop. It first executes then checks for the condition.
        It works fine but sometimes the board bugs when it enters a heavy loop */
        /*  do
            {
            int seed = rand() % 1000;
            srand(seed);
            playNum = (rand() % 2) + 1;
            row = rand() % 2; 
            } while (board[playNum]=='X' || board[playNum]=='O'); */
        }
    }
}