const main = document.querySelector('main');
const sorter = document.createElement("button")
const table = document.createElement('table');
const thead = document.createElement('thead');
const tbody = document.createElement('tbody');
const headrow = document.createElement('tr');
const games = document.createElement('th');
const playerNames = document.createElement('th');
const scores = document.createElement('th');
const moves = document.createElement('th');
const wol = document.createElement('th');
const del = document.createElement('th');
const submitBtn = document.getElementById("button");

//Entitling headers
games.innerText = 'Game ID';
playerNames.innerText = 'Names';
scores.innerText = 'Scores';
moves.innerText = 'Moves';
wol.innerText = 'Wins/Losses';
del.innerText = '';
//add ths to the thead
headrow.appendChild(games);
headrow.appendChild(playerNames);
headrow.appendChild(scores);
headrow.appendChild(moves);
headrow.appendChild(wol);
headrow.appendChild(del);
thead.appendChild(headrow);
table.appendChild(thead);
table.appendChild(tbody);
//getting THs and appending a button (sorter) to them
let headEls = [...headrow.getElementsByTagName("th")];
headEls.forEach((th)=> {
    if((th.innerText!=="") && (th.innerText!=="Wins/Losses")){
        const sorter = document.createElement("button");
        sorter.innerText="<";
        sorter.style.transform="rotate(-90deg)";
        sorter.classList.add("sort-btn");
        th.appendChild(sorter);
    }
})
//Creating divs to use for semantics
const divElem = document.createElement('div');
const divContainer = document.createElement('div');
main.appendChild(divContainer);
divElem.appendChild(table);
divElem.classList.add("table-box");
divContainer.classList.add("row");
divContainer.appendChild(divElem);
divElem.style.width = '80%';
divElem.style.margin = 'auto';
const formEl = document.getElementById('searchForm');

url = "http://localhost:63342/index.html/WebProject/rest/db.json";

//fetch Function


//general filter function that calls particular filtering functions (check down below)
const filterData = (game) => {
    if (formEl.elements.score.value !== "") {
        return filterOnScores(game);
    } else if (formEl.elements.pname.value !== "") {
        return filterOnName(game);
    } else if (formEl.elements.wol.value !== "") {
        return filterOnWoL(game);
    } else if (formEl.elements.moves.value !== "") {
        return filterOnMoves(game);
    }
    else
        return game.game_id === parseInt(formEl.elements.gameid.value)
}
async function fetchData() {
    const response = await fetch(url);
    const db_data = await response.json();
    processData(db_data.games);
}
const processData = (gamesList => {
    let data = gamesList.filter(game => filterData(game));
    tbody.innerHTML = ""
    data.forEach(({game_id, player_name, moves, player_score, win_or_loss}) => {
        const {row, gameTd, playerTd, pmovesTd, pscoreTd, pwolTd, delBtn} = fillTable();
        gameTd.innerText = game_id;
        playerTd.innerText = player_name;
        pmovesTd.innerText = moves;
        pscoreTd.innerText = player_score;
        pwolTd.innerText = win_or_loss;
        delBtn.addEventListener("mouseover", (e) => {
            e.target.classList.add("delete-btn:hover");
        })
        delBtn.addEventListener("click", (e) => {
            row.remove();
            e.stopPropagation();
        })
        row.addEventListener('mouseover', mOver());
        row.addEventListener('mouseout', mOut());
        delBtn.classList.add("delete-btn");

        formEl.reset()

        function mOver() {
            return () => {
                playerTd.style.color = 'gold';
            };
        }
        function mOut() {
            return () => {
                playerTd.style.color = '';
            }
        }
    })
})

//Filling the table with the retrieved data
function fillTable() {
    const row = document.createElement('tr');
    const gameTd = document.createElement('td');
    const playerTd = document.createElement('td');
    const pmovesTd = document.createElement('td');
    const pscoreTd = document.createElement('td');
    const pwolTd = document.createElement('td');
    const deleteBtnTd = document.createElement('td');
    const delBtn = document.createElement('button');
    delBtn.innerText = "X";
    deleteBtnTd.appendChild(delBtn);
    deleteBtnTd.style.textAlign = "center";
    row.appendChild(gameTd);
    row.appendChild(playerTd);
    row.appendChild(pscoreTd);
    row.appendChild(pmovesTd);
    row.appendChild(pwolTd);
    row.appendChild(deleteBtnTd);
    tbody.appendChild(row);
    return {row, gameTd, playerTd, pmovesTd, pscoreTd, pwolTd, delBtn};
}
//Filters
function filterOnName(game) {
    const namePattern = new RegExp(formEl.elements.pname.value.toLowerCase().trim());
    if (formEl.elements.wol.value !== "" && formEl.elements.pname.value !== "") {
        return namePattern.test(game.player_name.toLowerCase())
            && game.win_or_loss.toLowerCase() === formEl.elements.wol.value.toLowerCase()
            || game.player_score === parseInt(formEl.elements.score.value)
            || game.moves === parseInt(formEl.elements.moves.value);
    } else if (formEl.elements.moves.value !== "" && formEl.elements.pname.value !== "") {
        return namePattern.test(game.player_name.toLowerCase())
            && game.moves === parseInt(formEl.elements.moves.value)
            || game.player_score === parseInt(formEl.elements.score.value)
            || game.win_or_loss.toLowerCase() === formEl.elements.wol.value.toLowerCase();
    } else if (formEl.elements.score.value !== "" && formEl.elements.pname.value !== "") {
        return namePattern.test(game.player_name.toLowerCase())
            && game.player_score === parseInt(formEl.elements.score.value)
            || game.win_or_loss.toLowerCase() === formEl.elements.wol.value.toLowerCase()
            || game.moves === parseInt(formEl.elements.moves.value);
    } else {
        return namePattern.test(game.player_name.toLowerCase())
    }
}

function filterOnScores(game) {
    const namePattern = new RegExp(formEl.elements.pname.value.toLowerCase().trim());
    if (formEl.elements.score.value !== "" && formEl.elements.wol.value !== "") {
        return game.player_score === parseInt(formEl.elements.score.value)
            && game.win_or_loss.toLowerCase() === formEl.elements.wol.value.toLowerCase()
            || namePattern.test(game.player_name.toLowerCase())
            || game.moves === parseInt(formEl.elements.moves.value);
    } else if (formEl.elements.score.value !== "" && formEl.elements.moves.value !== "") {
        return game.player_score === parseInt(formEl.elements.score.value)
            && game.moves === parseInt(formEl.elements.moves.value)
            || namePattern.test(game.player_name.toLowerCase())
            || game.win_or_loss.toLowerCase() === formEl.elements.wol.value.toLowerCase();
    } else
        return game.player_score === parseInt(formEl.elements.score.value);

}

function filterOnWoL(game) {
    const namePattern = new RegExp(formEl.elements.pname.value.toLowerCase());
    if (formEl.elements.wol.value !== "" && formEl.elements.score.value !== "") {
        return game.win_or_loss.toLowerCase() === formEl.elements.wol.value.toLowerCase()
            && game.moves === parseInt(formEl.elements.moves.value)
            || namePattern.test(game.player_name.toLowerCase())
            || game.player_score === parseInt(formEl.elements.score.value);
    } else
        return game.win_or_loss.toLowerCase() === formEl.elements.wol.value.toLowerCase();
}

function filterOnMoves(game) {
    {
        if (formEl.elements.moves.value !== "" && formEl.elements.score.value !== "") {
            return game.moves === parseInt(formEl.elements.moves.value)
                && game.player_score === parseInt(formEl.elements.score.value)
        } else {
            return game.moves === parseInt(formEl.elements.moves.value)
        }
    }
}



/**
 * Sorts a HTML table.
 *
 * @param {HTMLTableElement} table tThe table to sort
 * @param {number} column The index of the column to sort
 * @param {boolean} asc Determines if the sorting will be in ascending order
 *
 * */
function sortTableByColumn(table, column, asc=true) {//ascending by default
    const dirModifier = asc ? 1 : -1;
    const tBody = table.tBodies[0];
    const rows = Array.from(tBody.querySelectorAll('tr'));//Array of TR (elements) instead of a node list

//Sort each row
    const sortedRows = rows.sort((a, b) => {
        let aColVal = a.querySelector(`td:nth-child(${column+1}`).textContent.trim();
        let bColVal = b.querySelector(`td:nth-child(${column+1}`).textContent.trim();
        if(!isNaN(parseInt(aColVal) || !isNaN(parseInt(bColVal)))){
            aColVal = parseInt(aColVal);
            bColVal = parseInt(bColVal);
        }
        return aColVal> bColVal ? (dirModifier):(-1*dirModifier);
    });
    // remove all existing TRs from the table
    while(tBody.fistChild){
        tBody.removeChild(tBody.firstChild)
    }
    // re-add the sorted rows
    tBody.append(...sortedRows);
    //register what kind of current sorting is on
    //clearning tokens
    table.querySelectorAll('th')
        .forEach(th => th.classList.remove("th-sort-asc","th-sort-desc"));
    //Tokens for reference
    table.querySelector(`th:nth-child(${column+1})`).classList.toggle("th-sort-asc",asc);
    table.querySelector(`th:nth-child(${column+1})`).classList.toggle("th-sort-desc",!asc);
}

//Selecting the buttons on each header and adding event listeners to them (to handle sorting)
table.querySelectorAll("th button ").forEach(headerButton => {
    headerButton.addEventListener("click",()=>{
        const tableElement =  headerButton.parentElement.parentElement.parentElement.parentElement;
        const headerCell = headerButton.parentElement;
        const headerCellIndex = Array.prototype
            .indexOf.call(headerCell.parentElement.children, headerCell);
        const currentIsAscending = headerButton.parentElement.classList.contains("th-sort-asc");
        let deg = 180;
        if(currentIsAscending){deg*=-1}
        headerButton.style.transform+=` rotate(${deg}deg)`
        sortTableByColumn(tableElement, headerCellIndex, !currentIsAscending); //toggle sorting
    })
})


//Submission handler
submitBtn.addEventListener('click', async (e) => {
    e.preventDefault();
    fetchData();
})


