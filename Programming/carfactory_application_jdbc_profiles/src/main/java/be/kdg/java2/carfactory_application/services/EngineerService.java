package be.kdg.java2.carfactory_application.services;
import be.kdg.java2.carfactory_application.domain.Engineer;

import java.util.List;

public interface EngineerService {

    Engineer addEngineer(Engineer engineer);

    List<Engineer> getAllEngineers();
    List<Engineer> getEngineerOnTradeMark(String trademark);
}
