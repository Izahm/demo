package be.kdg.java2.carfactory_application.presentation;


import be.kdg.java2.carfactory_application.domain.Car;
import be.kdg.java2.carfactory_application.domain.Engineer;
import be.kdg.java2.carfactory_application.presentation.dto.EngineerDTO;
import be.kdg.java2.carfactory_application.repository.CarRepository;
import be.kdg.java2.carfactory_application.repository.EngineerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Controller
@RequestMapping("/engineers")
public class EngineerController {

    private final EngineerRepository engineerRepository;
    private final CarRepository carRepository;
    Logger logger = LoggerFactory.getLogger(EngineerController.class);


    public EngineerController(EngineerRepository engineerRepository, CarRepository carRepository) {
        this.engineerRepository = engineerRepository;
        this.carRepository = carRepository;
    }

    @GetMapping
    public String showAllEnginners(Model model) {
        model.addAttribute("engineers", engineerRepository.read());
        logger.debug("displaying all engineers...");
        return "engineers";
    }

    @GetMapping("/{id}")
    public String engineerDetails(@PathVariable int id, Model model) {
        Engineer engineerbyId = engineerRepository.findById(id);
//        model.addAllAttributes(Map.of("engineer", engineerbyId, "cars", engineerbyId.getCars()));
        model.addAttribute("engineer", engineerbyId);
        model.addAttribute("cars", engineerbyId.getCars());
        return "engineerdetails";
    }

    @GetMapping("/edit/{id}")
    public String editEngineerDetails(@PathVariable int id, Model model) {
        Engineer engineerbyId = engineerRepository.findById(id);
        //remove the cars which are already selected (to avoid repetition)
        List<Car> availableCars = carRepository.read();
        availableCars.removeAll(engineerbyId.getCars());
//        that works too
//        model.addAllAttributes(Map.of("engineer", engineerbyId, "cars", engineerbyId.getCars(), "availcars", availableCars));
        model.addAttribute("engineer", engineerbyId);
        model.addAttribute("cars", engineerbyId.getCars());
        model.addAttribute("availcars", availableCars);
        return "engineerform";
    }
    //PutMapping missing...

    @GetMapping("/new")
    public String newEngineerForm(Model model) {
        model.addAttribute("engineer", new Engineer());
        model.addAttribute("cars", carRepository.read());
        return "engineerform";
    }

    @PostMapping("/new")
    public String processAddEngineer(@Valid @ModelAttribute("engineer") EngineerDTO dto, BindingResult result, Model model) {
        if (result.hasErrors()) {
            result.getAllErrors().forEach(error -> {
                logger.error(result.toString());
            });
            model.addAttribute("cars", carRepository.read());
            return "engineerform";
        }
        List<Car> cars = new ArrayList<>();
        dto.getContributionsIds().forEach((id) -> cars.add(carRepository.findById(id)));
        Engineer engineer = new Engineer(dto.getName(), dto.getTenure(), dto.getNationality());
        engineer.setCars(cars);
        engineerRepository.create(engineer);
        logger.debug("processing the new engineer item creation...");
        return "redirect:/engineers/" + engineer.getId();
    }

}
