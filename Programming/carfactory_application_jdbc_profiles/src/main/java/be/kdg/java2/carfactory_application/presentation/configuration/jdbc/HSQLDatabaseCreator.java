package be.kdg.java2.carfactory_application.presentation.configuration.jdbc;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
@Component
@Profile("JDBC")
public class HSQLDatabaseCreator {
    private static final Logger log = LoggerFactory.getLogger(HSQLDatabaseCreator.class);

    private JdbcTemplate jdbcTemplate;

    public HSQLDatabaseCreator(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @PostConstruct
    public void loadData() {
        jdbcTemplate.update("DROP TABLE IF EXISTS CARS");
        jdbcTemplate.update("CREATE TABLE CARS(CAR_ID INTEGER NOT NULL IDENTITY, " +
                "MODEL VARCHAR(100) NOT NULL," +
                "ENGINE_SIZE DOUBLE NOT NULL," +
                "PRICE INTEGER NOT NULL," +
                "RELEASE_DATE DATE NOT NULL," +
                "COLOR VARCHAR(100) NOT NULL)");
        jdbcTemplate.update("INSERT INTO CARS(CAR_ID, MODEL,\n" +
                "                 ENGINE_SIZE, PRICE, RELEASE_DATE, COLOR) " +
                "VALUES (0, '7 Series', 4.4, 134000, '2021-10-01', 'GRAY'),\n" +
                "       (1, '8 Series', 4.0, 14000, '2022-02-04', 'BLACK')");
        log.debug("JDBC profile used to create data");
    }
}
