package be.kdg.java2.carfactory_application.repository;

import be.kdg.java2.carfactory_application.domain.Car;
import be.kdg.java2.carfactory_application.domain.Color;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.Locale;

@Repository
@Component
@Profile("List")
public class CarRepository extends ItemsRepository<Car> {
    private static final Logger log = LoggerFactory.getLogger(CarRepository.class);

    public CarRepository() {
        log.debug("Creating car entities...");
    }


}
