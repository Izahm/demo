package be.kdg.java2.carfactory_application.repository;

import be.kdg.java2.carfactory_application.domain.Engineer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

@Repository
@Component
@Profile("List")
public class EngineerRepository extends ItemsRepository<Engineer> {
    private static final Logger log = LoggerFactory.getLogger(CarRepository.class);

    public EngineerRepository() {
        log.debug("Creating engineer entities.");
    }
}
