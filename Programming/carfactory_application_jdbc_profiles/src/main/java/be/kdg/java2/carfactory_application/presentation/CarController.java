package be.kdg.java2.carfactory_application.presentation;

import be.kdg.java2.carfactory_application.domain.Car;
import be.kdg.java2.carfactory_application.domain.Color;
import be.kdg.java2.carfactory_application.domain.Engineer;
import be.kdg.java2.carfactory_application.domain.TradeMark;
import be.kdg.java2.carfactory_application.presentation.dto.CarDTO;
import be.kdg.java2.carfactory_application.presentation.dto.TradeMarkDTO;
import be.kdg.java2.carfactory_application.repository.CarRepository;
import be.kdg.java2.carfactory_application.repository.EngineerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Controller
@RequestMapping("/cars")
public class CarController<T> {

    private final EngineerRepository engineerRepository;
    private final CarRepository carRepository;
    private static final Logger logger = LoggerFactory.getLogger(CarController.class);


    public CarController(EngineerRepository engineerRepository, CarRepository carRepository) {
        this.engineerRepository = engineerRepository;
        this.carRepository = carRepository;
    }

    @GetMapping
    public String showAllCars(Model model) {
        model.addAttribute("cars", carRepository.read());
        logger.debug("displaying all cars...");
        return "cars";
    }

    @GetMapping("/{id}")
    public String carDetails(@PathVariable int id, Model model) {
        model.addAttribute("car", carRepository.findById(id));
        return "cardetails";
    }

    @GetMapping("/edit/{id}")
    public String editCarDetails(@PathVariable int id, Model model) {
        Car carById = carRepository.findById(id);
        model.addAttribute("colors", Color.values());
        model.addAttribute("trademark", carById.getTradeMark());
        model.addAttribute("car", carById);
        //        carRepository.update(car);
        return "editcar";
    }

    //PutMapping missing...
//    @RequestMapping(value = "/ed/{id}", method = RequestMethod.PUT)
//    public String updateCar(@RequestBody Car car, @RequestBody TradeMark tradeMark) {
//        car.setTradeMark(tradeMark);
//        carRepository.update(car);
//        return "redirect:/cars";
//    }

    @GetMapping("/new")
    public String newCarForm(Model model) {
        model.addAttribute("car", new Car());
        model.addAttribute("trademark", new TradeMark());
        model.addAttribute("colors", Color.values());
        model.addAttribute("enginners", engineerRepository.read());
        return "carform";
    }

    @PostMapping("/new")
    public String processAddCar(@Valid @ModelAttribute("car") CarDTO dto, BindingResult carResult, @Valid @ModelAttribute("trademark") TradeMarkDTO tradeMarkDTO, BindingResult tdResult, Model model) {
        if (carResult.hasErrors() || tdResult.hasErrors()) {
            carResult.getAllErrors().forEach(error -> {
                logger.error(carResult.toString());
            });
            tdResult.getAllErrors().forEach(error -> {
                logger.error(tdResult.toString());
            });
            model.addAllAttributes(Map.of("colors", Color.values(), "enginners", engineerRepository.read()));
            return "carform";
        }
        List<Engineer> engineers = new ArrayList<>();
        dto.getEngineersIds().forEach((id) -> engineers.add(engineerRepository.findById(id)));
        Car car = new Car(dto.getModel(), dto.getEngineSize(), dto.getPrice(), dto.getReleaseDate(), dto.getColor(), engineers, dto.getImageUrl());
        TradeMark tradeMark = new TradeMark(tradeMarkDTO.getTitle(), tradeMarkDTO.getFounder(), tradeMarkDTO.getLaunchYear());
        car.setTradeMark(tradeMark);
        tradeMark.getCars().add(car);
        logger.debug("processing the new car item creation...");
        carRepository.create(car);
        return "redirect:/cars/" + car.getId();
    }
}