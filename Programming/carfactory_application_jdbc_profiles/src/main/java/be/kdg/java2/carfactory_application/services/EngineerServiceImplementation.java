package be.kdg.java2.carfactory_application.services;


import be.kdg.java2.carfactory_application.domain.Engineer;
import be.kdg.java2.carfactory_application.repository.EngineerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Component
public class EngineerServiceImplementation implements EngineerService {

    private final EngineerRepository engineerRepository;

    @Autowired
    public EngineerServiceImplementation(EngineerRepository engineerRepository) {
        this.engineerRepository = engineerRepository;
    }

    @Override
    public Engineer addEngineer(Engineer engineer) {
        return engineerRepository.create(engineer);
    }

    @Override
    public List<Engineer> getAllEngineers() {
        return engineerRepository.read();
    }

    @Override
    public List<Engineer> getEngineerOnTradeMark(String trademark) {
        Predicate<Engineer> filterOnCriterium;
        if (trademark == null || trademark.isEmpty()) {
            return new ArrayList<>();
        }
        filterOnCriterium = engineer -> engineer.getCars().stream().anyMatch(car -> car.getTradeMark().getTitle().toLowerCase(Locale.ROOT).contains(trademark.toLowerCase(Locale.ROOT)));
        return engineerRepository.read().stream()
                .filter(filterOnCriterium)
                .collect(Collectors.toList());
    }

}

