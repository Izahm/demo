package be.kdg.java2.carfactory_application.domain;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class TradeMark extends Entity {
    private final int currentYear = Calendar.getInstance().get(Calendar.YEAR);

    private String title;
    private String founder;
    private int launchYear;

    private final transient List<Car> cars = new ArrayList<>();
    public TradeMark() {

    }

    public TradeMark(String title, String founder, int launchDate) {
        this.title = title;
        this.founder = founder;
        this.launchYear = launchDate;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setLaunchYear(int launchYear) {
        this.launchYear = launchYear;
    }

    public String getTitle() {
        return title;
    }

    public void setName(String name) {
        this.title = name;
    }

    public String getFounder() {
        return founder;
    }

    public void setFounder(String founder) {
        this.founder = founder;
    }

    public int getLaunchYear() {
        return launchYear;
    }

    public void setLaunchYear(LocalDate launchYear) {
        launchYear = launchYear;
    }

    public List<Car> getCars() {
        return cars;
    }

    @Override
    public String toString() {
        return "Trademark" + title + '\n' +
                "Founder='" + founder + '\n' +
                "LaunchDate=" + launchYear;
    }
}
