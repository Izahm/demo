
const btn = document.querySelector('#v2');

// btn.addEventListener('click', function (e){
//  alert('Item clicked')
// })
//
function func() {
  alert('Item clicked')
}




// class Student {
//   constructor(name) {
//     if (!name) {
//       throw new ReferenceError('\'name\' is mandatory!');
//     } else if (typeof name !== 'string') {
//       throw new TypeError('\'name\' should be string!');
//     }
//     this._name = name;
//   }
// }
//
// const s1 = new Student(null, '123');
// const s2 = new Student(undefined, '123');
// const s3 = new Student('', '123');
// const s4 = new Student(7, '123');
// const s5 = new Student('Mary', '123');
//
//
//






class Address {
  constructor(recipient, postalCode, city, country) {
    this._recipient = recipient;
    this._postalCode = postalCode;
    this._city = city;
    this._country = country;
  }

  get recipient() {
    return this._recipient;
  }

  print() {
    console.log(`${this._postalCode} ${this._city}`);
    console.log(this._country.toUpperCase());
  }
}

class HomeAddress extends Address {
  constructor(recipient, street, houseNr, postalCode, city, country) {
    super(recipient, postalCode, city, country);
    this._street = street;
    this._houseNr = houseNr;
  }

  print() {
    console.log(this.recipient);
    console.log(`${this._street} ${this._houseNr}`);
    super.print();
  }
}

class POBox extends Address {
  static PO_BOX_PREFIX = 'PO Box';
  constructor(recipient, poBoxNr, postalCode, city, country, name) {
    super(recipient, postalCode, city, country);
    this._poBoxNr = poBoxNr;
    this._king = name;
  }

  print() {
    console.log(this.recipient);
    console.log(`${POBox.PO_BOX_PREFIX} ${this._poBoxNr}`);
    super.print();
  }
}

const home = new HomeAddress('Karel de Grote University of Applied Sciences and Arts',
    'Nationalestraat', 5, 2000, 'Antwerpen', 'Belgium');
const poBox = new POBox('KdG', 9001, 2000, 'Antwerpen', 'Belgium');

home.print();
console.log();
poBox.print();
console.log();

console.log('Is \'home\' an instance of Address? ' + (home instanceof Address));
console.log('Is \'home\' an instance of HomeAddress? ' + (home instanceof HomeAddress));
console.log('Is \'home\' an instance of POBox? ' + (home instanceof POBox));















// const box = document.createElement('div')
// box.style.width= '50px'
// box.style.height= '50px'
// box.style.backgroundColor='red'
// box.style.position='relative'
// box.style.top='50px'
// box.style.bottom='0'
// box.style.left='0'
// box.style.right='0'
// document.body.appendChild(box)
//
// window.addEventListener('keydown',(e)=> {
//  console.log(e.code);
//  console.log(box.style.top)
// if(e.code === "ArrowRight"){
//   box.style.left ='50px'
// }else if(e.code==="ArrowLeft"){
//   box.style.right ='50px'
// }else if(e.code=== "ArrowDown"){
//   box.style.top ='50px'
// }else if(e.code==="ArrowUp"){
//   box.style.bottom ='50px'
// }
//
// })
// box.offsetR




//
// const btn = document.querySelector("#btn")
// document.body.addEventListener('click',(eve)=>{
//   console.log(eve.clientX)
// })

// const loop = (num, boolFunc, updateFunction, callBack) => {
//   for (let i = num; boolFunc(i); i=updateFunction(i)){
//     callBack(i)
//   }
// }




// for (let i = 0; i < 20; i++) {
//     const btn = document.createElement('button')
//     btn.innerText = "Click"
//     document.body.appendChild(btn)
// }
//
// const randomRGB = () => {
//     const R = Math.floor(Math.random() * 256)
//     const G = Math.floor(Math.random() * 256)
//     const B = Math.floor(Math.random() * 256)
//     return `rgb(${R},${G},${B})`
// }
// const buttons = document.querySelectorAll('button')
//
// for (let button of buttons) {
//     button.addEventListener('click', function(){
//         this.style.backgroundColor = randomRGB();
//     })
// }


// const btn = document.querySelector("#btn")
// const h1 = document.querySelector('h1')
//
// const randomRGB = ()=> {
//     const R = Math.floor(Math.random()*256)
//     const G = Math.floor(Math.random()*256)
//     const B = Math.floor(Math.random()*256)
//     return `rgb(${R},${G},${B})`
// }
//
// const randomColor = randomRGB();
//
// document.body.bgColor = randomColor
// h1.innerText= randomColor
//
// btn.addEventListener('click',()=>{
//     const randomColor = randomRGB();
//     document.body.bgColor = randomColor
//     h1.innerText= randomColor
// })


// const btn = document.querySelector('#v2');
//
// btn.onclick = function () {
//     console.log("YOU CLICKED ME!")
//     console.log("I HOPE IT WORKED!!")
// }
//
// function scream() {
//     console.log("AAAAAHHHHH");
//     console.log("STOP TOUCHING ME!")
// }
//
// btn.onmouseenter = scream;
//
//
// document.querySelector('h1').onclick = () => {
//     alert('you clicked the h1!')
// }
//
//
// const btn3 = document.querySelector('#v3');
// btn3.addEventListener('click', function () {
//     alert("CLICKED!");
// })
//
// function twist() {
//     console.log("TWIST!")
// }
// function shout() {
//     console.log("SHOUT!")
// }
//
// const tasButton = document.querySelector('#tas');
//
// // tasButton.onclick = twist;
// // tasButton.onclick = shout;
//
// tasButton.addEventListener('click', twist)
// tasButton.addEventListener('click', shout)


//const btn = document.querySelector('#v2')

//const h1 = document.querySelector('h1')

//btn.addEventListener('click',() => console.log('hello'));
