const usernameInput = document.getElementById("username");
const secretInput = document.getElementById("pw");
const secretConfirmation= document.getElementById("pw-confirmation");
const dateInput = document.getElementById("date-of-birth");
const submitBtn = document.querySelector("input[type='submit']");


const userNameCheck =  document.querySelector('#username ~ p');
const secretCheck =  document.querySelector('#pw ~ p');
const confirmationCheck =  document.querySelector('#pw-confirmation ~ p');
const submissionCheck =  submitBtn.nextElementSibling;

const namePattern = /^[A-z]+[A-z0-9]{3,}$/;
const secretPattern = /^[A-z]+[A-z0-9]{3,}$/;


const processUserName = () =>{
    const username = usernameInput.value;
    const found = namePattern.test(username);
        if(username === ""){
          userNameCheck.innerText= "";
        }
        else if(!found){
          userNameCheck.innerText= "Wrong pattern";
        }
        else{
            userNameCheck.innerText= "";
        }
}

const processSecret = () =>{

    const secret = secretInput.value;
    const found = secretPattern.test(secret);
    if(secret===""){
        secretCheck.innerText= "";
    }
    else if(!found){
        secretCheck.innerText= "Wrong pattern";
    }else{
        secretCheck.innerText= "";
    }
}

const processConfirmation= ()=> {
    const confirmation = secretConfirmation.value;
    if(confirmation===""){
        confirmationCheck.innerText= "";
    }
    else if(secretInput.value !== confirmation){
        confirmationCheck.innerText= "Not the same password";
    }
    else if(secretInput.value === confirmation){
        confirmationCheck.style.color="green";
        confirmationCheck.innerText= "Correct match";
    }
}


const currentDate = new Date();
console.log(currentDate);


const myFunction = ()=>{
    let date = dateInput.value;
    date.
    console.log(date);
}
const btn = document.getElementById("try")
btn.addEventListener('click',myFunction)


const processDate = ()=>{
    const date = dateInput.value;
    const currentDate = new Date();
}



const saveData = (eve)=>{
    const namePass = namePattern.test(usernameInput.value);
    const secretPass = secretPattern.test(secretInput.value);
    const confirmationPass = secretConfirmation.value ==="";

    if( namePass && secretPass && confirmationPass ){
        localStorage.setItem('username',usernameInput.value);
    }
    else{
        eve.preventDefault();
        submissionCheck.style.color="red";
        submissionCheck.innerText="Please check your form before submission";
    }
}

submitBtn.addEventListener('click', saveData);


secretConfirmation.addEventListener('keyup',processConfirmation)
secretInput.addEventListener('keyup',processSecret);
usernameInput.addEventListener('keyup',processUserName);