const main = document.querySelector('main');
const table =document.createElement('table');
const thead =document.createElement('thead');
const tbody = document.createElement('tbody');
const headrow = document.createElement('tr');
const title= document.createElement('td');
const author= document.createElement('td');
title.innerText='Title';
author.innerText='Author';

headrow.appendChild(title);
headrow.appendChild(author);
thead.appendChild(headrow)

table.appendChild(thead);
table.appendChild(tbody);

main.appendChild(table)


const params = new URLSearchParams(location.search);

fetch(`https://www.googleapis.com/books/v1/volumes?${params.toString()}`).then(initialPromise=>{
    if(initialPromise.ok){
        return initialPromise.json();
    }
    throw new Error("Input invalid!")

}).then(jsonPromise=> {
    jsonPromise.items.forEach(({volumeInfo}) => {
        const row = document.createElement('tr');
        const title = document.createElement('td');
        const author = document.createElement('td');
        row.appendChild(title);
        row.appendChild(author);
        tbody.appendChild(row);
        title.innerText = volumeInfo.title;
        author.innerText= volumeInfo.authors.reduce((accum, current) =>{
            return `${accum}, ${current}`;
        });
    } )
})
