package be.kdg.java2.carfactory_application.presentation;


import be.kdg.java2.carfactory_application.domain.Engineer;
import be.kdg.java2.carfactory_application.repository.DataFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("/engineers")
public class EngineerController {
    private final DataFactory repository;
    Logger logger = LoggerFactory.getLogger(EngineerController.class);

    public EngineerController(DataFactory repository) {
        this.repository = repository;
    }

    @GetMapping
    public String showAllEnginners(Model model) {
        model.addAttribute("engineers",repository.readEngineers());
        logger.debug("displaying all engineers...");
        return "engineers";
    }
    @GetMapping("engineer/new")
    public String newEngineerForm(Model model){
        model.addAttribute("engineer", new Engineer());
        model.addAttribute("contributions", repository.readEngineers());
        return "addengineerform";
    }

    @PostMapping("engineer")
    public String processAddCar(Engineer engineer){
        repository.createEngineer(engineer);
        logger.debug("processing the new engineer item creation...");
        return "redirect:/engineers";
    }
}
