package be.kdg.java2.carfactory_application.presentation;

import be.kdg.java2.carfactory_application.domain.Car;
import be.kdg.java2.carfactory_application.services.CarService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
@RequestMapping( "/cars")

public class CarController {
    private final CarService carService;
    Logger logger = LoggerFactory.getLogger(CarController.class);

    public CarController(CarService carService) {
        this.carService = carService;
    }

    @GetMapping
    public String showAllCars(Model model) {
        logger.debug("displaying all cars...");
        model.addAttribute("cars", carService.getAllCars());
        return "cars";
    }

    @GetMapping("/info")
    public String showDogInfo(Model model){
        return "carsinfo";
    }

    @GetMapping("car/new")
    public String newCarForm(Model model) {
        model.addAttribute("car", new Car());
        return "addcarform";
    }

    @PostMapping("car")
    public String processAddCar(Car car) {
        logger.debug("processing the new car item creation...");
        carService.addCar(car);
        return "redirect:/cars";
    }
}