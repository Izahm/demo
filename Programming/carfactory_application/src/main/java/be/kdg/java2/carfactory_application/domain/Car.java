package be.kdg.java2.carfactory_application.domain;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;


public class Car {
    public enum Color {
        CHROME, WHITE, BLACK, BLUE, GRAY, YELLOW
    }
    private String model;
    private TradeMark tradeMark;
    private double engineSize;
    private long price;
    private transient LocalDate releaseDate;
    private Color color;
    //    link to engineers that worked on the car
    private transient List<Engineer> engineers = new ArrayList<>();

    public Car() {

    }
    public Car(String model, double engineSize, long price, LocalDate releaseDate, Color color, TradeMark tradeMark) {
        this.model = model;
        this.engineSize = engineSize;
        this.releaseDate = releaseDate;
        this.color = color;
        this.tradeMark = tradeMark;
        this.price = price;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public double getEngineSize() {
        return engineSize;
    }

    public void setEngineSize(double engineSize) {
        this.engineSize = engineSize;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(LocalDate releaseDate) {
        this.releaseDate = releaseDate;
    }

    public List<Engineer> getEngineers() {
        return engineers;
    }

    public TradeMark getTradeMark() {
        return this.tradeMark;
    }

    public long getPrice() {
        return price;
    }

    public Color getColor() {
        return color;
    }

    public void addEngineer(Engineer engineer) {
        engineers.add(engineer);
    }


    public String toString() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd LLLL yyyy");
        String formattedDate = releaseDate.format(formatter);
        StringBuilder engineerNames = new StringBuilder();
        for (Engineer engineer : engineers) {
            engineerNames.append(engineer.getName()).append(", ");
        }
        engineerNames.delete(engineerNames.length() - 2, engineerNames.length());

        return String.format("Trademark: %s%n" +
                "Model: %s%n" +
                "Engine: %.1fL%n" +
                "Release date: %s%n" +
                "Color: %s%n" +
                "Price: %s$%n" +
                "Engineers: %s%n" +
                "------------------------------", tradeMark.getName(), model, engineSize, formattedDate, this.color.toString(), price,engineerNames);

    }

}
