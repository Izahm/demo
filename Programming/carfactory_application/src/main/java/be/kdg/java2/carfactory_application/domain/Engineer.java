package be.kdg.java2.carfactory_application.domain;

import java.util.ArrayList;
import java.util.List;

public class Engineer {
    private String name;
    private int tenure;
    private String nationality;
    private List<Car> cars = new ArrayList<>();

    public Engineer(String name, int tenure, String nationality) {
        this.name = name;
        this.tenure = tenure;
        this.nationality = nationality;
    }

    public Engineer() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTenure() {
        return tenure;
    }

    public void setTenure(int tenure) {
        this.tenure = tenure;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public List<Car> getCars() {
        return cars;
    }

    public void setCars(List<Car> cars) {
        this.cars = cars;
    }

    public void addCar(Car car) {
        cars.add(car);
    }

    @Override
    public String toString() {

        StringBuilder carsNames = new StringBuilder();
        for (Car car : cars) {
            carsNames.append(car.getModel()).append(", ");
        }
        carsNames.delete(carsNames.length() - 2, carsNames.length());

        return
                "Name: " + name + '\n' +
                "Tenure: " + tenure + " years\n" +
                "Nationality: " +
                 nationality + "\nWork: "+ carsNames + "\n------------------------------";
    }
}
