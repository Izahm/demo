package be.kdg.java2.carfactory_application.presentation.dto;

import be.kdg.java2.carfactory_application.domain.Car;
import be.kdg.java2.carfactory_application.domain.Entity;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class TradeMarkDTO extends Entity {
    private String title;
    private String founder;
    private int launchYear;
    private final transient List<Car> cars = new ArrayList<>();

    public void setTitle(String title) {
        this.title = title;
    }

    public void setLaunchYear(int launchYear) {
        this.launchYear = launchYear;
    }

    public String getTitle() {
        return title;
    }

    public void setName(String name) {
        this.title = name;
    }

    public String getFounder() {
        return founder;
    }

    public void setFounder(String founder) {
        this.founder = founder;
    }

    public int getLaunchYear() {
        return launchYear;
    }

    public void setLaunchYear(LocalDate launchYear) {
        launchYear = launchYear;
    }

    public List<Car> getCars() {
        return cars;
    }

    @Override
    public String toString() {
        return "Trademark" + title + '\n' +
                "Founder='" + founder + '\n' +
                "LaunchDate=" + launchYear;
    }
}
