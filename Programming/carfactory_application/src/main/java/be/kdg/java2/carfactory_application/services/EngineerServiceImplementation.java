package be.kdg.java2.carfactory_application.services;



import be.kdg.java2.carfactory_application.domain.Engineer;
import be.kdg.java2.carfactory_application.repository.DataFactory;
import be.kdg.java2.carfactory_application.repository.DataFactoryHC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Component
public class EngineerServiceImplementation implements EngineerService {

    private final DataFactory dataFactory;

    @Autowired
    public EngineerServiceImplementation(DataFactory dataFactory) {
        this.dataFactory = dataFactory;
    }

    @Override
    public Engineer addEngineer(Engineer engineer) {
        return dataFactory.createEngineer(engineer);
    }

    @Override
    public List<Engineer> getAllEngineers() {
        return dataFactory.readEngineers();
    }

    @Override
    public List<Engineer> getEngineerOnTradeMark(String trademark) {
        Predicate<Engineer> filterOnCriterium;
        if (trademark == null || trademark.isEmpty()) {
            return new ArrayList<>();
        }
        filterOnCriterium = engineer -> engineer.getCars().stream().anyMatch(car -> car.getTradeMark().getName().toLowerCase(Locale.ROOT).contains(trademark.toLowerCase(Locale.ROOT)));
        return DataFactoryHC.engineers.stream()
                .filter(filterOnCriterium)
                .collect(Collectors.toList());
    }

}

