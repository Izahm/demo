package be.kdg.java2.carfactory_application.domain;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class TradeMark {
    private String name;
    private String founder;
    private final int launchDate;
    private final transient List<Car> cars = new ArrayList<>();

    public TradeMark(String name, String founder, int launchDate) {
        this.name = name;
        this.founder = founder;
        this.launchDate = launchDate;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFounder() {
        return founder;
    }

    public void setFounder(String founder) {
        this.founder = founder;
    }

    public int getLaunchDate() {
        return launchDate;
    }

    public void setLaunchDate(LocalDate launchDate) {
        launchDate = launchDate;
    }

    public List<Car> getCars() {
        return cars;
    }

    @Override
    public String toString() {
        return "Trademark" + name + '\n' +
                "Founder='" + founder + '\n' +
                "LaunchDate=" + launchDate;
    }
}
