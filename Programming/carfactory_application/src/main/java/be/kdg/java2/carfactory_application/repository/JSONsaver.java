package be.kdg.java2.carfactory_application.repository;



import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class JSONsaver {

    public <obj> JSONsaver(List<obj> data, String name) {
        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting();
        Gson gson = builder.create();
        try (FileWriter fileWriter = new FileWriter(name + ".json")) {
            if(!data.isEmpty())
                System.out.println("Saving to "+ name+".json");
                fileWriter.write(gson.toJson(data));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //Todo: I have no idea why when i try to save the filtered(data) objects one by one (using loop), it only saves one object to json file
    // Does it have anything to do with stream enclosing? (just a thought)
    /*
    public JSONsaver(Object obj) {
        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting();
        Gson gson = builder.create();

        try (FileWriter fileWriter = new FileWriter(obj.getClass().getSimpleName() + ".json")) {
            fileWriter.write(gson.toJson(obj));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    */

}
