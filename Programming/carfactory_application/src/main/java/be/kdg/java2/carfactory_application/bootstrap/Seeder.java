package be.kdg.java2.carfactory_application.bootstrap;

import be.kdg.java2.carfactory_application.domain.Car;
import be.kdg.java2.carfactory_application.domain.Engineer;
import be.kdg.java2.carfactory_application.domain.TradeMark;
import be.kdg.java2.carfactory_application.repository.DataFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import java.util.Arrays;
import java.util.List;


@Component
public class Seeder implements CommandLineRunner {
   private final DataFactory dataFactory;
   private static Logger logger = LoggerFactory.getLogger(Seeder.class);

    @Autowired
    public Seeder(DataFactory dataFactory) {
        this.dataFactory = dataFactory;
    }


    @Override
    public void run(String... args) throws Exception {
        logger.debug("Seeding data...");
        TradeMark mercedes = new TradeMark("Mercedes-Benz", "Karl Benz", 1926);
        TradeMark bmw = new TradeMark("BMW", "Camillo Castiglioni", 1916);
        TradeMark audi = new TradeMark("Audi", "August Horch", 1909);
        TradeMark volkswagen = new TradeMark("Volkswagen", "German Labour Front", 1937);
        TradeMark ford = new TradeMark("Ford", "Henry Ford", 1903);
        TradeMark toyota = new TradeMark("Toyota", "AKiichiro Toyoda", 1937);

        Car classA = new Car("A-Class", 2.0, 32500, java.time.LocalDate.of(2019, 8, 31), Car.Color.WHITE, mercedes);
        Car classC = new Car("C-Class", 2.0, 41400, java.time.LocalDate.of(2019, 12, 14), Car.Color.CHROME, mercedes);
        Car audiR8 = new Car("Audi R8", 4.0, 143000, java.time.LocalDate.of(2018, 1, 4), Car.Color.BLUE, audi);
        Car bmw7 = new Car("BMW 7 Series", 4.4, 134000, java.time.LocalDate.of(2021, 10, 1), Car.Color.CHROME, bmw);
        Car bmw8 = new Car("BMW 8 Series", 4.0, 14000, java.time.LocalDate.of(2022, 2, 4), Car.Color.BLACK, bmw);
        Car golf8 = new Car("Golf 8", 2.0, 25300, java.time.LocalDate.of(2021, 2, 5), Car.Color.GRAY, volkswagen);
        Car chr = new Car("C-HR", 4.8, 25300, java.time.LocalDate.of(2021, 2, 5), Car.Color.GRAY, toyota);
        Car fordSE = new Car("Fiesta SE", 1.2, 16000, java.time.LocalDate.of(2017, 6, 10), Car.Color.WHITE, ford);

        Engineer frank = new Engineer("Frank Lamberty", 18, "German");
        Engineer christopher = new Engineer("Christopher E. Bangle", 10, "American");
        Engineer christian = new Engineer("Christian Früh", 21, "German");
        Engineer rishi = new Engineer("Rishi Chavda", 15, "German");
        Engineer wonJu = new Engineer("Won Ju", 12, "Korean");
        Engineer bern = new Engineer("Bern Hard", 7, "American");
        Engineer muller = new Engineer("Heren Muller", 10, "German");
        Engineer korn = new Engineer("Winter Korn", 14, "American");


        classA.getEngineers().addAll(List.of(frank, christian));
        classC.getEngineers().addAll(List.of(christian, korn));
        audiR8.addEngineer(christopher);
        bmw7.getEngineers().addAll(List.of(rishi, wonJu));
        bmw8.addEngineer(wonJu);
        golf8.addEngineer(muller);
        fordSE.addEngineer(korn);
        chr.addEngineer(bern);


        dataFactory.readCars().addAll(Arrays.asList(classA, classC, audiR8, bmw7, golf8, chr, fordSE));
        mercedes.getCars().addAll(Arrays.asList(classA, classC));
        bmw.getCars().add(bmw7);
        audi.getCars().add(audiR8);
        volkswagen.getCars().add(golf8);
        toyota.getCars().add(chr);
        ford.getCars().add(fordSE);

        dataFactory.readEngineers().addAll(Arrays.asList(frank, christopher, christian, rishi, wonJu));
        frank.addCar(classA);
        christopher.addCar(audiR8);
        christian.getCars().addAll(List.of(classA, classC));
        rishi.addCar(bmw7);
        wonJu.getCars().addAll(List.of(bmw7, bmw8));
        bern.addCar(fordSE);
        muller.addCar(golf8);
        korn.addCar(chr);

        System.out.println(dataFactory.readEngineers());
    }
}
