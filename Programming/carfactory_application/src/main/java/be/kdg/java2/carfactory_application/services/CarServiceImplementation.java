package be.kdg.java2.carfactory_application.services;



import be.kdg.java2.carfactory_application.domain.Car;
import be.kdg.java2.carfactory_application.repository.DataFactory;
import be.kdg.java2.carfactory_application.repository.DataFactoryHC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Component
public class CarServiceImplementation implements CarService {

    private final DataFactory dataFactory;

    @Autowired
    public CarServiceImplementation(DataFactory dataFactory) {
        this.dataFactory = dataFactory;
    }

    @Override
    public Car addCar(Car car) {
        return dataFactory.createCar(car);
    }

    @Override
    public List<Car> getAllCars() {
        return dataFactory.readCars();
    }

    @Override
    public List<Car> getCarsOnModelOrTradeMark(String model, String tradeMark) {
        Predicate<Car> filterOnCriterium;
        if ((model == null || model.isEmpty()) && (tradeMark == null || tradeMark.isEmpty())) {
            return new ArrayList<>();
        }
        if (model == null || model.isEmpty()) {
            filterOnCriterium = car -> car.getTradeMark().getName().toLowerCase(Locale.ROOT).contains(tradeMark.toLowerCase(Locale.ROOT));
        } else {
            filterOnCriterium = car -> car.getModel().toLowerCase(Locale.ROOT).contains(model.toLowerCase(Locale.ROOT));
        }
        return DataFactoryHC.cars.stream()
                .filter(filterOnCriterium)
                .collect(Collectors.toList());
    }
}
