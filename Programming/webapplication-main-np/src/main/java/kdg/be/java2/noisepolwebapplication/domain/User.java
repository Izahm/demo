package kdg.be.java2.noisepolwebapplication.domain;

public class User extends Entity{
    private String username;
    private char[] password;
    private String email;
    private char gender;
    private Subscription subscription;
    //private Area ??
    //private String urlPic


    public User(String email) {
        this.email = email;
    }

    public User() {

    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public char getGender() {
        return gender;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    public Subscription getSubscription() {
        return subscription;
    }

    public void setSubscription(Subscription subscription) {
        this.subscription = subscription;
    }
}
