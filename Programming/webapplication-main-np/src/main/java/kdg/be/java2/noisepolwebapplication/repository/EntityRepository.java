package kdg.be.java2.noisepolwebapplication.repository;


import kdg.be.java2.noisepolwebapplication.domain.Entity;

import java.util.List;

public interface EntityRepository<T extends Entity> {
    List<T> read();
    T create(T t);
    T findById(int id);
    void delete(T t);
    void update(T t);
}
