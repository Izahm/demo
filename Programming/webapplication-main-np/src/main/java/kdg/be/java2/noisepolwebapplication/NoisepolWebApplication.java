package kdg.be.java2.noisepolwebapplication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NoisepolWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(NoisepolWebApplication.class, args);
    }

}
