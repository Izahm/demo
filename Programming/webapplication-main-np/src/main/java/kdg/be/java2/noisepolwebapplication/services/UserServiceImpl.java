package kdg.be.java2.noisepolwebapplication.services;

import kdg.be.java2.noisepolwebapplication.domain.User;
import kdg.be.java2.noisepolwebapplication.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private static final Logger logger= LoggerFactory.getLogger(UserServiceImpl.class);

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }


    @Override
    public User addUser() {
        return null;
    }

    @Override
    public List<User> getAllUsers() {
        logger.debug("Users are read...");
        return userRepository.read();
    }
}
