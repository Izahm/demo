package kdg.be.java2.noisepolwebapplication.domain;

public enum TicketType {
    QUESTION,FEEDBACK,COMPLAINT
}
