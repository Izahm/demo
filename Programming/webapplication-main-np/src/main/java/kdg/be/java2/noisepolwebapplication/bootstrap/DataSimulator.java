package kdg.be.java2.noisepolwebapplication.bootstrap;

import kdg.be.java2.noisepolwebapplication.domain.Ticket;
import kdg.be.java2.noisepolwebapplication.domain.TicketType;
import kdg.be.java2.noisepolwebapplication.presentation.MapController;
import kdg.be.java2.noisepolwebapplication.repository.TicketRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.Random;

@Component
public class DataSimulator implements CommandLineRunner {
    private final TicketRepository ticketRepository;

    private static final Logger log = LoggerFactory.getLogger(MapController.class);

    public DataSimulator(TicketRepository ticketRepository) {
        this.ticketRepository = ticketRepository;
    }

    public String randomString(){
        //Random text
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 30;

        Random random = new Random();
        StringBuilder buffer = new StringBuilder(targetStringLength);
        for (int i = 0; i < targetStringLength; i++) {
            int randomLimitedInt = leftLimit + (int)
                    (random.nextFloat() * (rightLimit - leftLimit + 1));
            buffer.append((char) randomLimitedInt);
        }
        return buffer.toString();
    }

    @Override
    public void run(String... args) {
        //Random ticket type
        TicketType randomType = TicketType.values()[new Random().nextInt(TicketType.values().length - 1)];
        log.debug("Seeding data...");
        for(int i =0; i< 10; i++){
            Ticket ticket = new Ticket(randomType, randomString(), new Random().nextInt(5),LocalDateTime.now(),false);
            ticketRepository.create(ticket);
        }
    }


}
