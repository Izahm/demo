package kdg.be.java2.noisepolwebapplication.services;

import kdg.be.java2.noisepolwebapplication.domain.Subscription;
import kdg.be.java2.noisepolwebapplication.repository.SubscriptionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class SubscriptionServiceImpl implements SubscriptionService {
    private SubscriptionRepository subscriptionRepository;
    private Logger logger= LoggerFactory.getLogger(SubscriptionServiceImpl.class);

    @Override
    public Subscription addSubscription() {
        Subscription subscription = new Subscription();
        //get methods

        logger.debug("Subscription is added...");
        return subscriptionRepository.create(subscription);
    }

    @Override
    public List<Subscription> getAllSubscriptions() {
        logger.debug("Subscriptions are read...");
        return subscriptionRepository.read();
    }
}
