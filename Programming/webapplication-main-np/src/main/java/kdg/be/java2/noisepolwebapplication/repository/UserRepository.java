package kdg.be.java2.noisepolwebapplication.repository;

import kdg.be.java2.noisepolwebapplication.domain.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class UserRepository extends ListRepository<User>{
    private Logger logger= LoggerFactory.getLogger(UserRepository.class);

    public UserRepository() {
        logger.debug("Creating User repository...");
    }

}
