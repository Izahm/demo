package kdg.be.java2.noisepolwebapplication.services;

import kdg.be.java2.noisepolwebapplication.domain.Ticket;
import kdg.be.java2.noisepolwebapplication.repository.TicketRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class TicketServiceImpl implements TicketService{
    private TicketRepository ticketRepository;
    private Logger logger= LoggerFactory.getLogger(TicketServiceImpl.class);

    @Autowired
    public TicketServiceImpl(TicketRepository ticketRepository) {
        this.ticketRepository = ticketRepository;
    }

    @Override
    public Ticket addTicket() {
        return null;
    }

    @Override
    public List<Ticket> getAllTickets() {
        logger.debug("Users are read...");
        return ticketRepository.read();
    }
}
