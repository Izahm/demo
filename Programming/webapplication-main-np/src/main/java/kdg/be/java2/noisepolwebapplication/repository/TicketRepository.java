package kdg.be.java2.noisepolwebapplication.repository;

import kdg.be.java2.noisepolwebapplication.domain.Ticket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class TicketRepository extends ListRepository<Ticket> {
    private Logger logger= LoggerFactory.getLogger(TicketRepository.class);
    public TicketRepository() {
        logger.debug("Creating Ticket repository...");
    }
}
