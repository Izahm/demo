let scoreEl = document.getElementById("score");
// Initialize the platform object:
const platform = new H.service.Platform({
    'apikey': 'DQI5ganRH-d6oYnh4zYsZzv5lAPDjZn6231wQE8u4jg'
});

// Obtain the default map types from the platform object
const maptypes = platform.createDefaultLayers();

// Instantiate (and display) a map object:
const map = new H.Map(
    document.getElementById('mapContainer'),
    maptypes.vector.normal.map,
    {
        zoom: 17,
        center: {lng: 4.4009, lat: 51.2182}
    });

let processLevel = (level => {
    if (level >= 60 && level <= 100) {
        return 'rgba(255,0,0,0.5)'
    } else if (level >= 50 && level <= 60) {
        return 'rgba(255,255,0,0.5)'
    } else if (level < 50) {
        return 'rgba(0,255,0,0.5)'
    }
})

const updateInterval = 1000;
let levels = []
let fillArray = (level) => {
    levels.push(level)
}

/* randomizer called every 1 second*/
setInterval(function () {
    fillArray(insertRandomDatapoints());
}, updateInterval);

/* Function that generates and inserts random datapoint into the processfunction */
function insertRandomDatapoints() {
    let tmpData = {
        one: Math.floor(Math.random() * 100) + 10
    };
    return tmpData.one;
}

const avg = arr => {
    const sum = arr.reduce((acc, cur) => acc + cur);
    return sum / arr.length;
}


file = '../filesystem/358853_2021-10-29T13-36-58.json'
//read date
const fetchData = async function (file) {
    const response = await fetch(file);
    const js_data = await response.json();
    processData(js_data);
}
fetchData(file)

let jsonLevels = []

//process data
const processData = function (data) {
    data.captures.forEach(capture => {
        jsonLevels.push(capture.level)
    })
}

let counter = 0;
setInterval(function () {
    let score = jsonLevels[counter]
    // let score = avg(levels)
    scoreEl.innerText ="Reading from JSON file "+  score.toFixed(2)
    let customStyle = {
        fillColor: processLevel(score)
    };
// Instantiate a circle object (using the default style):
    let circle = new H.map.Circle({lat: 51.2186, lng: 4.40135}, 40, {
        style: customStyle
    });
    // console.log(avg(levels))
    // console.log("Color changed to "+ customStyle.fillColor)
    // Add the circle to the map:
    map.addObject(circle);
    setTimeout(() => {
        map.removeObject(circle);
        // if(levels.length!==0)
        // if(levels)
        //     levels = []
        counter++;
            if (counter=== jsonLevels.length)
                counter=0;
    }, 2500)
}, 2500)

// setInterval(function () {
//     let score = avg(levels)
//     scoreEl.innerText = score.toFixed(2)
//     let customStyle = {
//         fillColor: processLevel(score)
//     };
// // Instantiate a circle object (using the default style):
//     let circle = new H.map.Circle({lat: 51.2186, lng: 4.40135}, 40, {
//         style: customStyle
//     });
//     // console.log(avg(levels))
//     // console.log("Color changed to "+ customStyle.fillColor)
//     // Add the circle to the map:
//     map.addObject(circle);
//     setTimeout(() => {
//         map.removeObject(circle);
//         // if(levels.length!==0)
//         if(levels)
//             levels = []
//     }, 10000)
// }, 10000)


//loop through files
// files.forEach(file => fetchData(file))





