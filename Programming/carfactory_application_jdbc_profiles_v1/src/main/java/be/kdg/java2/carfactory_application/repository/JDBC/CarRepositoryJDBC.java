package be.kdg.java2.carfactory_application.repository.JDBC;

import be.kdg.java2.carfactory_application.domain.Car;
import be.kdg.java2.carfactory_application.domain.Color;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Component
@Profile("JDBC")
public class CarRepositoryJDBC {
    private JdbcTemplate jdbcTemplate;
    private SimpleJdbcInsert carInserter;

    public CarRepositoryJDBC(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.carInserter = new SimpleJdbcInsert(jdbcTemplate)
                .withTableName("CARS")
                .usingGeneratedKeyColumns("CAR_ID");
    }

    //With method reference (mapRow)
    public Car findById(int carId) {
        return jdbcTemplate.queryForObject("SELECT * FROM CARS WHERE CAR_ID = ?",
                this::mapRow, carId);
    }

    private Car mapRow(ResultSet resultSet, int i) throws SQLException {
        return new Car(resultSet.getString("model"), resultSet.getDouble("engine_size"), resultSet.getInt("price"), resultSet.getDate("release_date").toLocalDate(), Color.valueOf(resultSet.getString("color").toUpperCase(Locale.ROOT)));
    }

    //Without method reference
//    public Car findById(int carId) {
//        return jdbcTemplate.queryForObject("SELECT * FROM CARS WHERE CAR_ID = ?",
//                (rs, rowNum) -> new Car(),carId);
//        //        Implicit return within lambda
//
//    }

    public Car save(Car car) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("model", car.getModel());
        parameters.put("engine_size", car.getEngineSize());
        parameters.put("price", car.getPrice());
        parameters.put("release_date", car.getReleaseDate());
        parameters.put("color", car.getColor());
        car.setId(carInserter.executeAndReturnKey(parameters).intValue());
        return car;
    }


    public List<Car> findAllCars() {
        String sql = "SELECT * FROM CARS";
        return jdbcTemplate.query(
                sql,
                (rs, rowNum) ->
                        new Car(
                                rs.getString("model"),
                                rs.getInt("engine_size"),
                                rs.getInt("price"),
                                rs.getDate("release_date").toLocalDate()
                                , Color.valueOf(rs.getString("color")))
        );

    }


}
