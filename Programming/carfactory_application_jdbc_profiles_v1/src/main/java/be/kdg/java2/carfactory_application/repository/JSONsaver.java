package be.kdg.java2.carfactory_application.repository;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class JSONsaver {
    private final static Logger LOGGER = LoggerFactory.getLogger(JSONsaver.class);
    private final Gson gson;
    public JSONsaver() {
        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting();
        this.gson = builder.create();
    }

    public <T> void write(List<T> data, String name) {
        try (FileWriter fileWriter = new FileWriter(name + ".json")) {
            if (!data.isEmpty())
                LOGGER.debug("Saving to " + name + ".json");
            fileWriter.write(gson.toJson(data));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //Todo: I have no idea why when i try to save the filtered(data) objects one by one (using loop), it only saves one object to json file
    // Does it have anything to do with stream enclosing? (just a thought)
    /*
    public JSONsaver(Object obj) {
        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting();
        Gson gson = builder.create();

        try (FileWriter fileWriter = new FileWriter(obj.getClass().getSimpleName() + ".json")) {
            fileWriter.write(gson.toJson(obj));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    */

}
