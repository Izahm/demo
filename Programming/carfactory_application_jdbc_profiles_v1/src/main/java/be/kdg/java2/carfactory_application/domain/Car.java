package be.kdg.java2.carfactory_application.domain;

import java.sql.Date;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;


public class Car extends Entity{

    private String imageUrl;
    private String model;
    private TradeMark tradeMark;
    private double engineSize;
    private int price;
    private transient LocalDate releaseDate;
    private Color color;
    //    link to engineers that worked on the car
    private transient List<Engineer> engineers = new ArrayList<>();

    public Car() {

    }

    public Car(String model, double engineSize, int price, LocalDate releaseDate, Color color, TradeMark tradeMark) {
        this.model = model;
        this.engineSize = engineSize;
        this.releaseDate = releaseDate;
        this.color = color;
        this.tradeMark = tradeMark;
        this.price = price;
    }

    public Car(String model, double engineSize, int price, LocalDate releaseDate, Color color, List<Engineer> engineers, String imageUrl) {
        this.model = model;
        this.engineSize = engineSize;
        this.price = price;
        this.releaseDate = releaseDate;
        this.color = color;
        this.engineers = engineers;
        this.imageUrl = imageUrl;
    }
    public Car(String model, double engineSize, int price, LocalDate releaseDate, Color color) {
        this.model = model;
        this.engineSize = engineSize;
        this.releaseDate = releaseDate;
        this.color = color;
        this.price = price;
    }


    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public double getEngineSize() {
        return engineSize;
    }

    public void setEngineSize(double engineSize) {
        this.engineSize = engineSize;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(LocalDate releaseDate) {
        this.releaseDate = releaseDate;
    }

    public void setTradeMark(TradeMark tradeMark) {
        this.tradeMark = tradeMark;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public List<Engineer> getEngineers() {
        return engineers;
    }

    public TradeMark getTradeMark() {
        return this.tradeMark;
    }

    public int getPrice() {
        return price;
    }

    public Color getColor() {
        return color;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public void addEngineer(Engineer engineer) {
        engineers.add(engineer);
    }



    public String toString() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        String formattedDate = releaseDate.format(formatter);

        return String.format(
                "Model: %s%n" +
                "Engine: %.1fL%n" +
                "Release date: %s%n" +
                "Color: %s%n" +
                "Price: %s$%n" +
                "------------------------------", model, engineSize, formattedDate, this.color.toString(), price);
    }

//    public String toString() {
//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd LLLL yyyy");
//        String formattedDate = releaseDate.format(formatter);
//        StringBuilder engineerNames = new StringBuilder();
//        for (Engineer engineer : engineers) {
//            engineerNames.append(engineer.getName()).append(", ");
//        }
//        engineerNames.delete(engineerNames.length() - 2, engineerNames.length());
//
//        return String.format("Trademark: %s%n" +
//                "Model: %s%n" +
//                "Engine: %.1fL%n" +
//                "Release date: %s%n" +
//                "Color: %s%n" +
//                "Price: %s$%n" +
//                "Engineers: %s%n" +
//                "------------------------------", tradeMark.getTitle(), model, engineSize, formattedDate, this.color.toString(), price,engineerNames);
//    }
//

}
