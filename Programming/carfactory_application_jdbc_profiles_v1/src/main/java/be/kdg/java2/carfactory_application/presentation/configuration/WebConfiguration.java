package be.kdg.java2.carfactory_application.presentation.configuration;

import be.kdg.java2.carfactory_application.presentation.converters.StringToLocalDateConverter;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfiguration implements WebMvcConfigurer {
    @Override
    public void addFormatters(FormatterRegistry registry){
        registry.addConverter(new StringToLocalDateConverter());
    }
}
