package be.kdg.java2.carfactory_application.repository;

import be.kdg.java2.carfactory_application.domain.Entity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public abstract class ItemsRepository<T extends Entity> implements EntityRepository<T> {
    private static final Logger log = LoggerFactory.getLogger(ItemsRepository.class);
    protected List<T> entities = new ArrayList<>();

    public ItemsRepository() {
        log.debug("Initiating repository...");
    }

    @Override
    public List<T> read() {
        log.debug("Reading entities...");
        return entities;
    }

    @Override
    public T findById(int id) {
        return entities.stream().filter(e -> e.getId() == id).findFirst().get();
    }

    @Override
    public T create(T entity) {
        log.debug("Creating enity : " + entity.getClass().getSimpleName() + " in " + this.getClass().getSimpleName());
        int maxId = entities.stream().mapToInt(Entity::getId).max().orElse(0);
        entity.setId(maxId + 1);
        entities.add(entity);
        return entity;
    }

    @Override
    public void delete(T entity) {
        log.debug("Deleting enity: " + entity.getClass().getSimpleName()+ " of id: "+ entity.getId());
        entities.remove(entity);
    }

    @Override
    public void update(T entity) {
        log.debug("Updating enity: " + entity.getClass().getSimpleName()+ " of id: "+ entity.getId());
        int index = entities.indexOf(entity);
        entities.set(index, entity);
    }
}