package be.kdg.java2.carfactory_application.services;



import be.kdg.java2.carfactory_application.domain.Car;
import be.kdg.java2.carfactory_application.repository.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Component
public class CarServiceImplementation implements CarService {

    private final CarRepository carRepository;

    @Autowired
    public CarServiceImplementation(CarRepository carRepository) {
        this.carRepository = carRepository;
    }

    @Override
    public Car addCar(Car car) {
        return carRepository.create(car);
    }

    @Override
    public List<Car> getAllCars() {
        return carRepository.read();
    }

    @Override
    public List<Car> getCarsOnModelOrTradeMark(String model, String tradeMark) {
        Predicate<Car> filterOnCriterium;
        if ((model == null || model.isEmpty()) && (tradeMark == null || tradeMark.isEmpty())) {
            return new ArrayList<>();
        }
        if (model == null || model.isEmpty()) {
            filterOnCriterium = car -> car.getTradeMark().getTitle().toLowerCase(Locale.ROOT).contains(tradeMark.toLowerCase(Locale.ROOT));
        } else {
            filterOnCriterium = car -> car.getModel().toLowerCase(Locale.ROOT).contains(model.toLowerCase(Locale.ROOT));
        }
        return carRepository.read().stream()
                .filter(filterOnCriterium)
                .collect(Collectors.toList());
    }
}
