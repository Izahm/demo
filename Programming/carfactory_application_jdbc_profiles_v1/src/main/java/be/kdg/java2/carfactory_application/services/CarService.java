package be.kdg.java2.carfactory_application.services;
import be.kdg.java2.carfactory_application.domain.Car;

import java.util.List;

public interface CarService {
    Car addCar(Car car);

    List<Car> getAllCars();

    List<Car> getCarsOnModelOrTradeMark(String model, String tradeMark);
}
