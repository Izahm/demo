package be.kdg.java2.carfactory_application;

import be.kdg.java2.carfactory_application.domain.Car;
import be.kdg.java2.carfactory_application.domain.Color;
import be.kdg.java2.carfactory_application.repository.JDBC.CarRepositoryJDBC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

@SpringBootApplication
public class CarfactoryApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(CarfactoryApplication.class, args);
    }

    @Autowired
    CarRepositoryJDBC carRepositoryJDBC;

    @Override
    public void run(String... args) throws Exception {

        System.out.print("Enter model: ");
        String model = new Scanner(System.in).next();
        System.out.print("Enter engine_size: ");
        double engine_size = new Scanner(System.in).nextDouble();
        System.out.print("Enter price: ");
        int price = new Scanner(System.in).nextInt();
        System.out.print("Enter release_date: ");
        String release_date = new Scanner(System.in).next();
        System.out.print("Enter color: ");
        String color = new Scanner(System.in).next();

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate release_localDate = LocalDate.parse(release_date, formatter);

        Car car = new Car(model, engine_size, price, release_localDate, Color.valueOf(color));
        carRepositoryJDBC.save(car);
        System.out.println("Car id: " + car.getId());

        System.out.println("All cars");
        carRepositoryJDBC.findAllCars().forEach(System.out::println);

    }
}
