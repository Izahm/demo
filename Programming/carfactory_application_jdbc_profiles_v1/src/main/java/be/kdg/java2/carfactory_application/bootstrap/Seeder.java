package be.kdg.java2.carfactory_application.bootstrap;

import be.kdg.java2.carfactory_application.domain.Car;
import be.kdg.java2.carfactory_application.domain.Color;
import be.kdg.java2.carfactory_application.domain.Engineer;
import be.kdg.java2.carfactory_application.domain.TradeMark;
import be.kdg.java2.carfactory_application.repository.CarRepository;
import be.kdg.java2.carfactory_application.repository.EngineerRepository;
import be.kdg.java2.carfactory_application.repository.JSONsaver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;


@Component
@Profile("List")
public class Seeder implements CommandLineRunner {

    private final CarRepository carRepository;
    private final EngineerRepository engineerRepository;
    private final static JSONsaver json = new JSONsaver();
    private static final Logger logger = LoggerFactory.getLogger(Seeder.class);


    @Autowired
    public Seeder(CarRepository carRepository, EngineerRepository engineerRepository) {
        this.carRepository = carRepository;
        this.engineerRepository = engineerRepository;
    }


    @Override
    public void run(String... args) throws Exception {
        logger.debug("Seeding data...");
        TradeMark mercedes = new TradeMark("Mercedes-Benz", "Karl Benz", 1926);
        TradeMark bmw = new TradeMark("BMW", "Camillo Castiglioni", 1916);
        TradeMark audi = new TradeMark("Audi", "August Horch", 1909);
        TradeMark volkswagen = new TradeMark("Volkswagen", "German Labour Front", 1937);
        TradeMark ford = new TradeMark("Ford", "Henry Ford", 1903);
        TradeMark toyota = new TradeMark("Toyota", "AKiichiro Toyoda", 1937);

        String classAs = "https://upload.wikimedia.org/wikipedia/commons/f/f7/2018_Mercedes-Benz_A200_AMG_Line_Premium_Automatic_1.3_Front.jpg";
        Car classA = new Car("A-Class", 2.0, 32500, java.time.LocalDate.of(2019, 8, 31), Color.WHITE, mercedes);
        classA.setImageUrl(classAs);

        String classCs = "https://upload.wikimedia.org/wikipedia/commons/5/52/Mercedes-Benz_C_200_Avantgarde_%28W_205%29_%E2%80%93_Frontansicht%2C_26._April_2014%2C_D%C3%BCsseldorf.jpg";
        Car classC = new Car("C-Class", 2.0, 41400, java.time.LocalDate.of(2019, 12, 14), Color.CHROME, mercedes);
        classC.setImageUrl(classCs);

        String audiR8s = "https://topgear.nl/thumbs/hd/2020/02/Audi-R8-V10-grijs.jpg";
        Car audiR8 = new Car("Audi R8", 4.0, 143000, java.time.LocalDate.of(2018, 1, 4), Color.BLUE, audi);
        audiR8.setImageUrl(audiR8s);

        String bmw7s = "https://carnetwork.s3.ap-southeast-1.amazonaws.com/file/3072b10496e24e57b15a47dfd16b5ab9.jpg";
        Car bmw7 = new Car("7 Series", 4.4, 134000, java.time.LocalDate.of(2021, 10, 1), Color.GRAY, bmw);
        bmw7.setImageUrl(bmw7s);


        String bmw8s = "https://www.bmw.co.uk/content/dam/bmw/marketGB/bmw_co_uk/bmw-cars/8-series/8series-grancoupe-modelcard-890x501.png";
        Car bmw8 = new Car("8 Series", 4.0, 14000, java.time.LocalDate.of(2022, 2, 4), Color.BLACK, bmw);
        bmw8.setImageUrl(bmw8s);

        String golf8s = "https://i2.wp.com/practicalmotoring.com.au/wp-content/uploads/2019/10/143f373b-2020-vw-golf-viii-mk8-8.jpg?fit=1800%2C992&ssl=1";
        Car golf8 = new Car("Golf 8", 2.0, 25300, java.time.LocalDate.of(2021, 2, 5), Color.WHITE, volkswagen);
        golf8.setImageUrl(golf8s);

        String chrs = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTpfFA2PlPP2oWX_5LLxcZNuxijNxeBGHds_kSNqW401omFzgZ5pUeMMSp71HfkP7MH0kc&usqp=CAU";
        Car chr = new Car("C-HR", 4.8, 25300, java.time.LocalDate.of(2021, 2, 5), Color.WHITE, toyota);
        chr.setImageUrl(chrs);

        String fordSEs = "https://bestcars.com.br/bc/wp-content/uploads/2013/07/Ford-Fiesta-Sedan-19.jpg";
        Car fordSE = new Car("Fiesta SE", 1.2, 16000, java.time.LocalDate.of(2017, 6, 10), Color.WHITE, ford);
        fordSE.setImageUrl(fordSEs);

        Engineer frank = new Engineer("Frank Lamberty", 18, "German");
        Engineer christopher = new Engineer("Christopher E. Bangle", 10, "American");
        Engineer christian = new Engineer("Christian Früh", 21, "German");
        Engineer rishi = new Engineer("Rishi Chavda", 15, "German");
        Engineer wonJu = new Engineer("Won Ju", 12, "Korean");
        Engineer bern = new Engineer("Bern Hard", 7, "American");
        Engineer muller = new Engineer("Heren Muller", 10, "German");
        Engineer korn = new Engineer("Winter Korn", 14, "American");


        classA.getEngineers().addAll(List.of(frank, christian));
        classC.getEngineers().addAll(List.of(christian, korn));
        audiR8.addEngineer(christopher);
        bmw7.getEngineers().addAll(List.of(rishi, wonJu));
        bmw8.addEngineer(wonJu);
        golf8.addEngineer(muller);
        fordSE.addEngineer(korn);
        chr.addEngineer(bern);

        //Cars creation
        for (Car car : Arrays.asList(classA, classC, audiR8, bmw7, bmw8, golf8, chr, fordSE)) {
            carRepository.create(car);
        }
        //        write to json file
        json.write(carRepository.read(), "cars");
//      add engineers to cars (workers list)
        mercedes.getCars().addAll(Arrays.asList(classA, classC));
        bmw.getCars().add(bmw7);
        audi.getCars().add(audiR8);
        volkswagen.getCars().add(golf8);
        toyota.getCars().add(chr);
        ford.getCars().add(fordSE);

//        Enginners creation
        for (Engineer engineer : Arrays.asList(frank, christopher, christian, rishi, wonJu)) {
            engineerRepository.create(engineer);
        }
//        write to json file
        json.write(engineerRepository.read(), "engineers");
//        add cars to engineers work list (cars)
        frank.addCar(classA);
        christopher.addCar(audiR8);
        christian.getCars().addAll(List.of(classA, classC));
        rishi.addCar(bmw7);
        wonJu.getCars().addAll(List.of(bmw7, bmw8));
        bern.addCar(fordSE);
        muller.addCar(golf8);
        korn.addCar(chr);


    }
}
