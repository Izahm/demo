DROP TABLE IF EXISTS CARS;
CREATE TABLE CARS(
                     CAR_ID INTEGER NOT NULL IDENTITY,
                     MODEL VARCHAR(100) NOT NULL,
                     ENGINE_SIZE DOUBLE NOT NULL,
                     PRICE INTEGER NOT NULL,
                     RELEASE_DATE DATE NOT NULL,
                     COLOR VARCHAR(100) NOT NULL,
--                   have multiple cars
--                      ENGINEER_ID INTEGER NOT NULL REFERENCES ENGINEERS (ENGINEER_ID)

);
-- DROP TABLE IF EXISTS ENGINEERS;
-- CREATE TABLE ENGINEERS(
--                      ENGINEER_ID INTEGER NOT NULL IDENTITY PRIMARY KEY,
--                      NAME VARCHAR(100) NOT NULL,
--                      TENURE INTEGER NOT NULL,
--                      NATIONALITY VARCHAR(100) NOT NULL,
--                      CAR INTEGER NOT NULL,
-- );
