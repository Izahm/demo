package kdg.be.java2.noisepolwebapplication.presentation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/map")
public class MapController {
    private Logger log = LoggerFactory.getLogger(MapController.class);

    public MapController() {
        log.debug("MapController is created...");
    }
    @GetMapping
    public String showMap(Model model){
        log.debug("Showing the map...");

        return "map";
    }
}
