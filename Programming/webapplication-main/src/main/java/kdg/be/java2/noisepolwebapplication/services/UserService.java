package kdg.be.java2.noisepolwebapplication.services;

import kdg.be.java2.noisepolwebapplication.domain.User;

import java.util.List;

public interface UserService {
    User addUser();
    List<User>  getAllUsers();

}
