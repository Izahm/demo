package kdg.be.java2.noisepolwebapplication.services;

import kdg.be.java2.noisepolwebapplication.domain.Ticket;

import java.util.List;

public interface TicketService {
    Ticket addTicket();
    List<Ticket> getAllTickets();
}
