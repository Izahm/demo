package kdg.be.java2.noisepolwebapplication.domain;

import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Ticket extends Entity{

    private TicketType type;
    private String text;
    private User sender;
    private int senderId;
    private LocalDateTime date;
    private boolean isProcessed;


    public Ticket(TicketType type, String text, int senderId, LocalDateTime date, boolean isProcessed) {
        this.type = type;
        this.text = text;
        this.senderId= senderId;
        this.date = date;
        this.isProcessed = isProcessed;
    }

    public Ticket() {

    }

    public int getSenderId() {
        return senderId;
    }

    public void setSenderId(int senderId) {
        this.senderId = senderId;
    }

    public TicketType getType() {
        return type;
    }

    public void setType(TicketType type) {
        this.type = type;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public boolean isProcessed() {
        return isProcessed;
    }

    public void setProcessed(boolean processed) {
        isProcessed = processed;
    }

    @Override
    public String toString() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd LLLL yyyy");
        String formattedDate = date.format(formatter);
        return "Ticket{" +
                "type=" + type +
                ", text='" + text + '\'' +
                ", sender=" + sender +
                ", date=" + date +
                ", isProcessed=" + isProcessed +
                "} " + super.toString();
    }
}
