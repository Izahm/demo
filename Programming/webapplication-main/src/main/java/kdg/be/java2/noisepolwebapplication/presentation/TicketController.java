package kdg.be.java2.noisepolwebapplication.presentation;

import kdg.be.java2.noisepolwebapplication.domain.Ticket;
import kdg.be.java2.noisepolwebapplication.domain.TicketType;
import kdg.be.java2.noisepolwebapplication.domain.User;
import kdg.be.java2.noisepolwebapplication.repository.TicketRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.time.LocalDateTime;
import java.util.Random;

@Controller
@RequestMapping("/tickets")
public class TicketController {
    private final TicketRepository ticketRepository;
    private static Logger log = LoggerFactory.getLogger(MapController.class);

    public TicketController(TicketRepository ticketRepository) {
        this.ticketRepository = ticketRepository;
    }

    @GetMapping
    public String showAllTickets(Model model) {
        model.addAttribute("tickets", ticketRepository.read());
        return "tickets";
    }

    @GetMapping("/new")
    public String createTicket(Model model) {
        model.addAttribute("user", new User());
        model.addAttribute("types", TicketType.values());
        return "ticketform";
    }

    @PostMapping("/new")
    public String processTicket(Ticket ticket, User user) {
        ticket.setProcessed(false);
        ticket.setDate(LocalDateTime.now());
        ticket.setSender(user);
        ticket.setSenderId(user.getId());
        ticketRepository.create(ticket);
        return "redirect:/tickets";
    }

}
