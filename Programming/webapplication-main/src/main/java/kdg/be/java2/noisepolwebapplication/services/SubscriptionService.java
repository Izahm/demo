package kdg.be.java2.noisepolwebapplication.services;

import kdg.be.java2.noisepolwebapplication.domain.Subscription;

import java.util.List;

public interface SubscriptionService {
    Subscription addSubscription();
    List<Subscription> getAllSubscriptions();
}
