package kdg.be.java2.noisepolwebapplication.repository;

import kdg.be.java2.noisepolwebapplication.domain.Subscription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class SubscriptionRepository extends ListRepository<Subscription>{
    private Logger logger= LoggerFactory.getLogger(TicketRepository.class);
    public SubscriptionRepository() {
        logger.debug("Creating Subscription repository...");
    }
}
