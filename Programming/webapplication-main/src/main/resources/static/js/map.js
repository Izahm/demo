
//Initialize the Platform object:
let platform = new H.service.Platform({
    'apikey': 'DQI5ganRH-d6oYnh4zYsZzv5lAPDjZn6231wQE8u4jg'
});

// Get the default map types from the Platform object:
let defaultLayers = platform.createDefaultLayers();

// Instantiate the map:
let map = new H.Map(
    document.getElementById('mapContainer'),
    defaultLayers.vector.normal.map,
    {
        zoom: 10,
        center: {lng: 4.402771, lat: 51.260197}
    });

// Create the default UI:
let ui = H.ui.UI.createDefault(map, defaultLayers);


// Enable the event system on the map instance:
let mapEvents = new H.mapevents.MapEvents(map);

// Add event listeners:
map.addEventListener('tap', function (evt) {
    // Log 'tap' and 'mouse' events:
    console.log(evt.type, evt.currentPointer.type);
});

// Instantiate the default behavior, providing the mapEvents object:
let behavior = new H.mapevents.Behavior(mapEvents);


let processLevel = (level => {
    if (level >= 60 && level <= 100) {
        return 'rgba(255,0,0,0.5)'
    } else if (level >= 50 && level <= 60) {
        return 'rgba(255,255,0,0.5)'
    } else if (level < 50) {
        return 'rgba(0,255,0,0.5)'
    }
})

const updateInterval = 1000;
let levels = []
let fillArray = (level) => {
    levels.push(level)
}

/* randomizer called every 1 second*/
setInterval(function () {
    fillArray(insertRandomDatapoints());
}, updateInterval);

/* Function that generates and inserts random datapoint into the processfunction */
function insertRandomDatapoints() {
    let tmpData = {
        one: Math.floor(Math.random() * 100) + 10
    };
    return tmpData.one;
}

const avg = arr => {
    const sum = arr.reduce((acc, cur) => acc + cur);
    return sum / arr.length;
}


setInterval(function () {
    let customStyle = {
        fillColor: processLevel(avg(levels))
    };
// Instantiate a circle object (using the default style):
    let circle = new H.map.Circle({lat: 51.260197, lng: 4.402771}, 8000, {
        style: customStyle
    });
    // console.log(avg(levels))
    // console.log("Color changed to "+ customStyle.fillColor)
    // Add the circle to the map:
    map.addObject(circle);
    setTimeout(() => {
        map.removeObject(circle);
        if (levels.length !== 0) levels = []
    }, 10000 - 100)
}, 10000)


let customStyle = {
    fillColor: 'rgba(239,236,85,0.79)'
};
// Instantiate a circle object (using the default style):
let circle = new H.map.Circle({lat: 50.9378, lng: 4.0410}, 1000, {
    style: customStyle
});
map.addObject(circle);

//Styling
// (not-needed-controls)
document.querySelector('div[title="Choose view"]').style.display= "none"
document.querySelector('.H_l_horizontal .H_ctl').style.display="none"
//map
document.getElementsByTagName("canvas")[0].addEventListener('mousedown',(eve)=>{
    eve.target.style.cursor="grabbing"
})
document.getElementsByTagName("canvas")[0].addEventListener('mouseup',(eve)=>{
    eve.target.style.cursor="grab"
})
