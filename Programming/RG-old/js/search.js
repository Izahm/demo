const main = document.querySelector('main');
const sorter = document.createElement("button")
const table = document.createElement('table');
const thead = document.createElement('thead');
const tbody = document.createElement('tbody');
const headrow = document.createElement('tr');
const games = document.createElement('th');
const playerNames = document.createElement('th');
const scores = document.createElement('th');
const moves = document.createElement('th');
const wol = document.createElement('th');
const del = document.createElement('th');

games.innerText = 'Game ID';
playerNames.innerText = 'Names';
scores.innerText = 'Scores';
moves.innerText = 'Moves';
wol.innerText = 'Wins/Losses';
del.innerText = 'Delete';

const submitBtn = document.getElementById("button");
headrow.appendChild(games);
headrow.appendChild(playerNames);
headrow.appendChild(scores);
headrow.appendChild(moves);
headrow.appendChild(wol);
headrow.appendChild(del);
thead.appendChild(headrow);
table.appendChild(thead);
table.appendChild(tbody);
let els = [...headrow.getElementsByTagName("th")]
els.forEach((th)=> {
    const sorter = document.createElement("button")
    sorter.innerText="<"
    sorter.style.transform="rotate(90deg)"
    th.appendChild(sorter)
})
const divElem = document.createElement('div');
const divContainer = document.createElement('div');
main.appendChild(divContainer);
divElem.appendChild(table);
divElem.classList.add("table-box");
divContainer.classList.add("row");
divContainer.appendChild(divElem);
divElem.style.width = '80%';
divElem.style.margin = 'auto';


const formEl = document.getElementById('searchForm');
const gameIdEl = document.getElementById('game_id');
const playerMovesEl = document.getElementById('pmoves');
const playerScoreEl = document.getElementById('pscore');
const playerNameEl = document.getElementById('pname');
const winOrLossEl = document.getElementById('win-or-loss');


const params = new URLSearchParams(location.search);
url = "http://localhost:63342/index.html/WebProject/rest/db.json";


async function fetchData() {
    const response = await fetch(url);
    const db_data = await response.json();
    processData(db_data.games);
}

const filterData = (game) => {
    if (formEl.elements.score.value !== "") {
        return filterOnScores(game);
    } else if (formEl.elements.pname.value !== "") {
        return filterOnName(game);
    } else if (formEl.elements.wol.value !== "") {
        return filterOnWoL(game);
    } else if (formEl.elements.moves.value !== "") {
        return filterOnMoves(game);
    }
    else
        return game.game_id === parseInt(formEl.elements.gameid.value)
}

const processData = (gamesList => {
    let data = gamesList.filter(game => filterData(game));
    tbody.innerHTML = ""
    data.forEach(({game_id, player_name, moves, player_score, win_or_loss}) => {
        const {row, gameTd, playerTd, pmovesTd, pscoreTd, pwolTd, delBtn} = forgeTable();

        gameTd.innerText = game_id;
        playerTd.innerText = player_name;
        pmovesTd.innerText = moves;
        pscoreTd.innerText = player_score;
        pwolTd.innerText = win_or_loss;

        delBtn.addEventListener("mouseover", (e) => {
            e.target.classList.add("delete-btn:hover")
        })
        delBtn.addEventListener("click", (e) => {
            row.remove();
            e.stopPropagation();
        })
        row.addEventListener('mouseover', mOver())
        row.addEventListener('mouseout', mOut())
        delBtn.classList.add("delete-btn")

        formEl.reset()

        function mOver() {
            return () => {
                playerTd.style.color = 'gold'
            };
        }
        function mOut() {
            return () => {
                playerTd.style.color = ''
            };
        }
    })
})
document.querySelectorAll('th button').forEach(button => button.addEventListener('click', () => {
    const table = button.closest('table');
    Array.from(table.querySelectorAll('tr:nth-child(n+2)'))
        .sort(comparer(Array.from(button.parentNode.children).indexOf(button), this.asc = !this.asc))
        .forEach(tr => table.appendChild(tr));
}));
const getCellValue = (tr, idx) => tr.children[idx].innerText || tr.children[idx].textContent;
const comparer = (idx, asc) => (a, b) => ((v1, v2) =>
        v1 !== '' && v2 !== '' && !isNaN(v1) && !isNaN(v2) ? v1 - v2 : v1.toString().localeCompare(v2)
)(getCellValue(asc ? a : b, idx), getCellValue(asc ? b : a, idx));



submitBtn.addEventListener('click', async (e) => {
    e.preventDefault()
    fetchData();
})

function forgeTable() {
    const row = document.createElement('tr');
    const gameTd = document.createElement('td');
    const playerTd = document.createElement('td');
    const pmovesTd = document.createElement('td');
    const pscoreTd = document.createElement('td');
    const pwolTd = document.createElement('td');
    const btnTd = document.createElement('td');
    const delBtn = document.createElement('button');
    delBtn.innerText = "delete";
    btnTd.appendChild(delBtn);
    btnTd.style.textAlign = "center";
    row.appendChild(gameTd);
    row.appendChild(playerTd);
    row.appendChild(pscoreTd);
    row.appendChild(pmovesTd);
    row.appendChild(pwolTd);
    row.appendChild(btnTd);
    tbody.appendChild(row);
    return {row, gameTd, playerTd, pmovesTd, pscoreTd, pwolTd, delBtn};
}

function filterOnName(game) {
    const namePattern = new RegExp(formEl.elements.pname.value.toLowerCase().trim());
    if (formEl.elements.wol.value !== "" && formEl.elements.pname.value !== "") {
        return namePattern.test(game.player_name.toLowerCase())
            && game.win_or_loss.toLowerCase() === formEl.elements.wol.value.toLowerCase()
            || game.player_score === parseInt(formEl.elements.score.value)
            || game.moves === parseInt(formEl.elements.moves.value);
    } else if (formEl.elements.moves.value !== "" && formEl.elements.pname.value !== "") {
        return namePattern.test(game.player_name.toLowerCase())
            && game.moves === parseInt(formEl.elements.moves.value)
            || game.player_score === parseInt(formEl.elements.score.value)
            || game.win_or_loss.toLowerCase() === formEl.elements.wol.value.toLowerCase();
    } else if (formEl.elements.score.value !== "" && formEl.elements.pname.value !== "") {
        return namePattern.test(game.player_name.toLowerCase())
            && game.player_score === parseInt(formEl.elements.score.value)
            || game.win_or_loss.toLowerCase() === formEl.elements.wol.value.toLowerCase()
            || game.moves === parseInt(formEl.elements.moves.value);
    } else {
        return namePattern.test(game.player_name.toLowerCase())
    }
}

function filterOnScores(game) {
    const namePattern = new RegExp(formEl.elements.pname.value.toLowerCase().trim());
    if (formEl.elements.score.value !== "" && formEl.elements.wol.value !== "") {
        return game.player_score === parseInt(formEl.elements.score.value)
            && game.win_or_loss.toLowerCase() === formEl.elements.wol.value.toLowerCase()
            || namePattern.test(game.player_name.toLowerCase())
            || game.moves === parseInt(formEl.elements.moves.value);
    } else if (formEl.elements.score.value !== "" && formEl.elements.moves.value !== "") {
        return game.player_score === parseInt(formEl.elements.score.value)
            && game.moves === parseInt(formEl.elements.moves.value)
            || namePattern.test(game.player_name.toLowerCase())
            || game.win_or_loss.toLowerCase() === formEl.elements.wol.value.toLowerCase();
    } else
        return game.player_score === parseInt(formEl.elements.score.value);

}

function filterOnWoL(game) {
    const namePattern = new RegExp(formEl.elements.pname.value.toLowerCase());
    if (formEl.elements.wol.value !== "" && formEl.elements.score.value !== "") {
        return game.win_or_loss.toLowerCase() === formEl.elements.wol.value.toLowerCase()
            && game.moves === parseInt(formEl.elements.moves.value)
            || namePattern.test(game.player_name.toLowerCase())
            || game.player_score === parseInt(formEl.elements.score.value);
    } else
        return game.win_or_loss.toLowerCase() === formEl.elements.wol.value.toLowerCase();
}

function filterOnMoves(game) {
    {
        if (formEl.elements.moves.value !== "" && formEl.elements.score.value !== "") {
            return game.moves === parseInt(formEl.elements.moves.value)
                && game.player_score === parseInt(formEl.elements.score.value)
        } else {
            return game.moves === parseInt(formEl.elements.moves.value)
        }
    }
}



























const filterWoL = (game) => {
    return game.win_or_loss.toLowerCase() === formEl.elements.wol.value.toLowerCase();
}
const filterMoves = (game) => {
    return game.moves <= parseInt(formEl.elements.moves.value);
}
const filterScores = (game) => {
    return game.player_score <= parseInt(formEl.elements.score.value);
}

const filterId = (game) => {
    if (formEl.elements.gameid.value >= 0)
        return game.game_id === parseInt(formEl.elements.gameid.value);
}
const filterName = (game) => {
    if (formEl.elements.pname.value) {
        const namePattern = new RegExp(formEl.elements.pname.value.toLowerCase());
        return namePattern.test(game.player_name.toLowerCase());
    }
}

const filterDatads = ({game_id, player_name, moves, player_score, win_or_loss}) => {
    if (params.get('gameid')) {
        return game_id === parseInt(params.get('gameid'));
    } else if (params.get('pscore')) {
        return player_score <= parseInt(params.get('pscore'));
    } else if (params.get('pmoves')) {
        return moves === parseInt(params.get('pmoves'));
    } else if (params.get('pname')) {
        const namePattern = new RegExp(params.get('pname').toLowerCase());
        return namePattern.test(player_name.toLowerCase());
    } else if (params.get('wol')) {
        return win_or_loss.toUpperCase() === params.get('wol').toUpperCase();
    }
}